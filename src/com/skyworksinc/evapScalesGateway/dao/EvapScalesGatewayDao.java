package com.skyworksinc.evapScalesGateway.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.skyworksinc.evapScalesGateway.beans.MetalUsageObject;
import javafx.collections.ObservableList;
import com.skyworksinc.evapScalesGateway.beans.EvapMeltPocket;
import java.util.Iterator;
import com.skyworksinc.evapScalesGateway.beans.EvapPocketInfo;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;
import com.skyworksinc.evapScalesGateway.beans.EvapMeltProperty;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import java.util.UUID;
import com.skyworksinc.evapScalesGateway.beans.PrepPocketStatusMessage;
import com.skyworksinc.evapScalesGateway.beans.EvapPrepState;

/**
 *
 * @author mattoop
 */
public class EvapScalesGatewayDao {
    
    String url="jdbc:oracle:thin:@(DESCRIPTION=(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=npcimrac01-vip) (PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=npcimrac02-vip) (PORT=1521))(CONNECT_DATA=(SERVICE_NAME=CIMRACN)))";
    String username="SWUSER";
    String password = "SWUSER";
    
    private Connection open() {
        Connection conn = null;
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            conn = DriverManager.getConnection(this.url, this.username, this.password);
        } catch (SQLException ex) {
            System.err.println(ex);
        }

        return conn;
    }
    
    public PrepPocketStatusMessage evapMakeNewMelt(String meltId, String eqpId, String material, double inWeight) throws SQLException, NamingException {
        PrepPocketStatusMessage prepPocketStatusMessage = null;
        try (Connection connection = open(); CallableStatement callSt = connection.prepareCall("{call EVAP_NEW_MELT(?,?,?,?,?,?)}")) {
            callSt.setString(1, meltId.trim());
            callSt.setString(2, eqpId.trim());
            callSt.setString(3, material.trim());
            callSt.setDouble(4, inWeight);
            callSt.registerOutParameter(5, Types.INTEGER);
            callSt.registerOutParameter(6, Types.VARCHAR);
            callSt.execute();
            String statusMessage = callSt.getString(6);
            int callStatus = callSt.getInt(5);
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.INFO, "EVAP_NEW_MELT Output: " + callStatus + " " + statusMessage);
            prepPocketStatusMessage = new PrepPocketStatusMessage(callStatus, callStatus > 0 ? statusMessage : "");
        } catch (SQLException sqle) {
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.SEVERE, null, sqle);
        }
        return prepPocketStatusMessage;
    }
    
    public void evapInvalidatePrep(String eqpId, String recipe, String prepId, String pocketNumber) throws SQLException, NamingException {
        try (Connection connection = open(); CallableStatement callSt = connection.prepareCall("{call INVALIDATE_PREP(?,?,?,?,?,?)}")) {
            callSt.setString(1, eqpId.trim());
            callSt.setString(2, recipe.trim());
            callSt.setString(3, prepId.trim());
            callSt.setString(4, pocketNumber.trim());
            callSt.registerOutParameter(5, Types.INTEGER);
            callSt.registerOutParameter(6, Types.VARCHAR);
            callSt.execute();
            String statusMessage = callSt.getString(6);
            int callStatus = callSt.getInt(5);
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.INFO, "INVALIDATE_PREP Output: " + callStatus + " " + statusMessage);
        } catch (SQLException sqle) {
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.SEVERE, null, sqle);
        }
    }
    
    public PrepPocketStatusMessage evapRemoveMelt(String meltId, String eqpId, String location, double meltWeight) throws SQLException, NamingException {
        PrepPocketStatusMessage prepPocketStatusMessage = null;
        try (Connection connection = open(); CallableStatement callSt = connection.prepareCall("{call EVAP_REMOVE_MELT(?,?,?,?,?,?)}")) {
            callSt.setString(1, meltId.trim());
            callSt.setString(2, eqpId.trim());
            callSt.setString(3, location.trim());
            callSt.setDouble(4, meltWeight);
            callSt.registerOutParameter(5, Types.INTEGER);
            callSt.registerOutParameter(6, Types.VARCHAR);
            callSt.execute();
            String statusMessage = callSt.getString(6);
            int callStatus = callSt.getInt(5);
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.INFO, "EVAP_REMOVE_MELT Output: " + callStatus + " " + statusMessage);
            prepPocketStatusMessage = new PrepPocketStatusMessage(callStatus, callStatus > 0 ? statusMessage : "");
        } catch (SQLException sqle) {
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.SEVERE, null, sqle);
        }
        return prepPocketStatusMessage;
    }
    
    public PrepPocketStatusMessage evapUpdateMelt(String meltId, String location, String qualStatus, double meltWeight) throws SQLException, NamingException {
        PrepPocketStatusMessage prepPocketStatusMessage = null;
        try (Connection connection = open(); CallableStatement callSt = connection.prepareCall("{call EVAP_UPDATE_MELT(?,?,?,?,?,?)}")) {
            callSt.setString(1, meltId.trim());
            callSt.setString(2, location.trim());
            callSt.setString(3, qualStatus.trim());
            callSt.setDouble(4, meltWeight);
            callSt.registerOutParameter(5, Types.INTEGER);
            callSt.registerOutParameter(6, Types.VARCHAR);
            callSt.execute();
            String statusMessage = callSt.getString(6);
            int callStatus = callSt.getInt(5);
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.INFO, "EVAP_UPDATE_MELT Output: " + callStatus + " " + statusMessage);
            prepPocketStatusMessage = new PrepPocketStatusMessage(callStatus, callStatus > 0 ? statusMessage : "");
        } catch (SQLException sqle) {
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.SEVERE, null, sqle);
        }
        return prepPocketStatusMessage;
    }
    
    public PrepPocketStatusMessage evapPrepPocket(String eqpId, String recipe, String pocketName, String meltId, double pocketWeight, double pocketAddMain, double pocketAddSub,
            String prepId, String userName) throws SQLException, NamingException {
        PrepPocketStatusMessage prepPocketStatusMessage = null;
        try (Connection connection = open(); CallableStatement callSt = connection.prepareCall("{call EVAP_PREP_POCKET(?,?,?,?,?,?,?,?,?,?,?)}")) {
            callSt.setString(1, eqpId.trim());
            callSt.setString(2, recipe.trim());
            callSt.setString(3, pocketName.trim());
            callSt.setString(4, meltId.trim());
            callSt.setDouble(5, pocketWeight);
            callSt.setDouble(6, pocketAddMain);
            callSt.setDouble(7, pocketAddSub);
            callSt.setString(8, prepId.trim());
            callSt.setString(9, userName.trim());
            callSt.registerOutParameter(10, Types.INTEGER);
            callSt.registerOutParameter(11, Types.VARCHAR);
            callSt.execute();
            String statusMessage = callSt.getString(11);
            int callStatus = callSt.getInt(10);
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.INFO, "EVAP_PREP_POCKET Output: " + callStatus + " " + statusMessage);
            prepPocketStatusMessage = new PrepPocketStatusMessage(callStatus, callStatus > 0 ? statusMessage : "");
        } catch (SQLException sqle) {
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.SEVERE, null, sqle);
        }
        return prepPocketStatusMessage;
    }
    
    public void updateToolState(String eqpId, String toolState, String toolRecipe) throws SQLException, NamingException {
        try (Connection connection = open(); CallableStatement callSt = connection.prepareCall("{call UPDATE_TOOL_STATE(?,?,?,?,?)}")) {
            callSt.setString(1, eqpId.trim());
            callSt.setString(2, toolState.trim());
            callSt.setString(3, toolRecipe.trim());
            callSt.registerOutParameter(4, Types.INTEGER);
            callSt.registerOutParameter(5, Types.VARCHAR);
            callSt.execute();
            String statusMessage = callSt.getString(5);
            int callStatus = callSt.getInt(4);
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.INFO, "UPDATE_TOOL_STATE Output: " + callStatus + " " + statusMessage);
        } catch (SQLException sqle) {
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.SEVERE, null, sqle);
        }
    }
    
    public Map<String, Boolean> ifAllowedToPrep(List<String> meltIds) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer queryBuffer = new StringBuffer();
        meltIds.removeIf(item -> item == null || "".equals(item));
        queryBuffer.append("SELECT MELT_ID, QUALSTATUS FROM EVAP_MELT_TRACKING WHERE MELT_ID IN (");    //? AND MATERIAL=?";
        for(int listIndex=0; listIndex < meltIds.size(); listIndex++) {
            queryBuffer.append(" ?");
            if(listIndex < meltIds.size()-1) {
                queryBuffer.append(",");
            }
        }
        queryBuffer.append(" ) ");
        Map<String, Boolean> meltIdToPrepStatusMap = new HashMap<>();
        String qualStatus = null;
        String meltId = null;
        int meltIdLoopIndex = 1;
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(queryBuffer.toString());
            for(int index=0; index < meltIds.size(); index++) {
                pstmt.setString(meltIdLoopIndex, meltIds.get(index));
                meltIdLoopIndex++;
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                qualStatus = rs.getString("QUALSTATUS");
                meltId = rs.getString("MELT_ID");
                if (qualStatus != null) {
                    if (qualStatus.equalsIgnoreCase("QUALIFIED") || qualStatus.equalsIgnoreCase("NOT_QUALIFIED")) {
                        meltIdToPrepStatusMap.put(meltId, true);
                    } else {
                        meltIdToPrepStatusMap.put(meltId, false);
                    }
                }
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from evap_melt_tracking: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return meltIdToPrepStatusMap;
    }
    
    public String fetchMeltWeight(String meltId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT WEIGHT FROM EVAP_MELT_TRACKING WHERE MELT_ID=?";
        String meltWeight = "";
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, meltId.trim());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                meltWeight = rs.getString("WEIGHT");
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from EVAP_MELT_TRACKING inside fetchMeltWeight: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return meltWeight;
    }
    
    public String fetchQualStatus(String meltId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT QUALSTATUS FROM EVAP_MELT_TRACKING WHERE MELT_ID=?";
        String qualstatus = "";
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, meltId.trim());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                qualstatus = rs.getString("QUALSTATUS");
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from EVAP_MELT_TRACKING inside fetchQualStatus: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return qualstatus;
    }
    
    public String fetchPrepTime(String eqpId, String pocketName) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT TO_CHAR(TIMESTAMP, 'DD-MON-YY HH24:MI:SS') AS PREP_TIME FROM EVAP_POCKETS WHERE EQPID=? AND POCKET_NAME=?";
        String prepTime = "";
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, eqpId.trim());
            pstmt.setString(2, pocketName.trim());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                prepTime = rs.getString("PREP_TIME");
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from evap_pockets inside fetchPrepTime: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return prepTime;
    }
    
    public String fetchPrepId(String eqpId, String pocketName) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = " SELECT prepid FROM EVAP_POCKETS WHERE EQPID=? AND POCKET_NAME=?";
        String prepId = null;
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, eqpId.trim());
            pstmt.setString(2, pocketName.trim());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                prepId = rs.getString("prepid");
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from evap_pockets inside fetchPrepId: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return prepId;
    }
    
    public boolean checkIfEvapIsPartiallyPrepped(String eqpId) {
        boolean isEvapPartiallyPrepped = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT COUNT(1) AS NOMELTCOUNT FROM EVAP_POCKETS_NO_MELTST WHERE PREPID IN (SELECT DISTINCT(PREPID) FROM EVAP_POCKETS WHERE EQPID=?) AND EQPID=? AND STATE='PREPPED'";
        int noPrepCount=-1;

        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, eqpId.trim());
            pstmt.setString(2, eqpId.trim());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                noPrepCount = rs.getInt("NOMELTCOUNT");
                if (noPrepCount > 0) {
                    isEvapPartiallyPrepped = true;
                }
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch (SQLException sqle) {
            System.err.println("Sql exception while selecting from EVAP_POCKETS_NO_MELTST inside checkIfEvapIsPartiallyPrepped: " + sqle.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return isEvapPartiallyPrepped;
    }
    
    private boolean checkIfEvapAlreadyPreppedWithNoMelt(String eqpId, String pocketName, String prepId) {
        boolean ifAlreadyPreppedWithNoMelt = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT COUNT(1) AS NOMELTCOUNT FROM EVAP_POCKETS_NO_MELTST WHERE POCKET_NAME=? AND EQPID=? AND STATE='PREPPED' AND PREPID=?";
        int noPrepCount=-1;

        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, pocketName.trim());
            pstmt.setString(2, eqpId.trim());
            pstmt.setString(3, prepId.trim());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                noPrepCount = rs.getInt("NOMELTCOUNT");
                if (noPrepCount > 0) {
                    ifAlreadyPreppedWithNoMelt = true;
                }
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch (SQLException sqle) {
            System.err.println("Sql exception while selecting from EVAP_POCKETS_NO_MELTST inside checkIfEvapAlreadyPreppedWithNoMelt: " + sqle.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return ifAlreadyPreppedWithNoMelt;
    }
    
    public String createOrRetrievePrepId(String eqpId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT POCKET_NAME, MELT_ID, STATE, PREPID FROM EVAP_POCKETS WHERE EQPID=?";
        String outPrepId = null;
        String state;
        String prepId = null;
        String meltId = null;
        String pocketName = null;
        List<EvapPrepState> evapPrepStateList = new ArrayList<>();
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, eqpId.trim());
            rs = pstmt.executeQuery();
            while(rs.next()) {
                state = rs.getString("STATE");
                prepId = rs.getString("PREPID");
                meltId = rs.getString("MELT_ID");
                pocketName = rs.getString("POCKET_NAME");
                evapPrepStateList.add(new EvapPrepState(prepId, state, meltId, pocketName));
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from evap_pockets inside createOrRetrievePrepId: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        
        Iterator<EvapPrepState> evapPrepStateIterator = evapPrepStateList.iterator();
        while(evapPrepStateIterator.hasNext()) {
            EvapPrepState evapPrepState = evapPrepStateIterator.next();
            if(evapPrepState.getState().equalsIgnoreCase("PREPPED")) {
                outPrepId = evapPrepState.getPrepId();
                break;
            }
        }
        if (outPrepId == null) {
            evapPrepStateIterator = evapPrepStateList.iterator();
            while (evapPrepStateIterator.hasNext()) {
                EvapPrepState evapPrepState = evapPrepStateIterator.next();
                if (evapPrepState.getMeltId() == null || evapPrepState.getMeltId().contains("_" + evapPrepState.getPocketName())) {
                    if (checkIfEvapAlreadyPreppedWithNoMelt(eqpId, evapPrepState.getPocketName(), evapPrepState.getPrepId())) {
                        outPrepId = evapPrepState.getPrepId();
                        break;
                    }
                }
            }
        }
        
        return outPrepId;
    }
    
    public PrepPocketStatusMessage updateEvapPocketWithNoMelt(String eqpId, String pocketName, String state, String prepId) throws SQLException, NamingException {
        PrepPocketStatusMessage prepPocketStatusMessage = null;
        try (Connection connection = open(); CallableStatement callSt = connection.prepareCall("{call EVAP_POCKET_NOMELT(?,?,?,?,?,?)}")) {
            callSt.setString(1, eqpId.trim());
            callSt.setString(2, pocketName.trim());
            callSt.setString(3, prepId.trim());
            callSt.setString(4, state.trim());
            callSt.registerOutParameter(5, Types.INTEGER);
            callSt.registerOutParameter(6, Types.VARCHAR);
            callSt.execute();
            String statusMessage = callSt.getString(6);
            int callStatus = callSt.getInt(5);
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.INFO, "EVAP_POCKET_NOMELT Output: " + callStatus + " " + statusMessage);
            prepPocketStatusMessage = new PrepPocketStatusMessage(callStatus, callStatus > 0 ? statusMessage : "");
        } catch (SQLException sqle) {
            Logger.getLogger(EvapScalesGatewayDao.class.getName()).log(Level.SEVERE, null, sqle);
        }
        return prepPocketStatusMessage;
    }
    
    
    public EvapPocketInfo fetchPocketWeightInfo(String eqpId, String pocketName) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "select eqpid, pocket_name, nvl(melt_id,eqpid || '_' || pocket_name) as meltId, to_char(timestamp,'mm/dd/yyyy hh24:mi') as meltTimestamp, state, prepid, "
                + " base_weight, main_add_weight, sub_add_weight from evap_pockets WHERE EQPID=? AND POCKET_NAME=?";
        EvapPocketInfo evapPocketInfo = null;
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, eqpId.trim());
            pstmt.setString(2, pocketName.trim());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                evapPocketInfo = new EvapPocketInfo(rs.getString("eqpid"), rs.getString("pocket_name"), rs.getString("meltId"), rs.getString("meltTimestamp"), rs.getString("state"),
                        UUID.randomUUID().toString(), rs.getDouble("base_weight"), rs.getDouble("main_add_weight"), rs.getDouble("sub_add_weight"));
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from evap_pockets inside fetchPocketWeightInfo: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return evapPocketInfo;
    }
    
    public Map<String, EvapMeltProperty> fetchPocketInfo(String evapId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "select pocket_name, nvl(melt_id,eqpid || '_' || pocket_name) AS meltid, state, BASE_WEIGHT, MAIN_ADD_WEIGHT, SUB_ADD_WEIGHT, PREPID from " +
            " evap_pockets where eqpid=?";
        Map<String, EvapMeltProperty> pocketToMeltIdPropertiesMap = new HashMap<>();
        double postWeight = 0.0;
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, evapId.trim());
            rs = pstmt.executeQuery();
            while(rs.next()) {
                double baseWeight = rs.getDouble("BASE_WEIGHT");
                double mainAddWeight = rs.getDouble("MAIN_ADD_WEIGHT");
                double subAddWeight = rs.getDouble("SUB_ADD_WEIGHT");
                String pocketName = rs.getString("pocket_name");
                //Don't include TA weight for AUGE prep
                if(pocketName.equalsIgnoreCase("AUGE")) {
                    postWeight = baseWeight+mainAddWeight;
                } else {
                    postWeight = baseWeight+mainAddWeight+subAddWeight;
                }
                String prepId = rs.getString("PREPID");
                pocketToMeltIdPropertiesMap.put(pocketName, new EvapMeltProperty(rs.getString("meltid"), rs.getString("state"), Double.toString(baseWeight), Double.toString(mainAddWeight), 
                        Double.toString(subAddWeight), Double.toString(postWeight), prepId));
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from evap_pockets inside fetchPocketInfo: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return pocketToMeltIdPropertiesMap;
    }
    
    public List<EvapMeltPocket> getMeltPocketForEvap(String evapId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT MELT, POCKET_NO FROM EVAP_MELT_DEDICATION WHERE TOOL_ID=?";
        List<EvapMeltPocket> evapMeltPocketList = new ArrayList<>();
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, evapId.trim());
            rs = pstmt.executeQuery();
            while(rs.next()) {
                evapMeltPocketList.add(new EvapMeltPocket(rs.getString("MELT"), rs.getString("POCKET_NO")));
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from EVAP_MELT_DEDICATION: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        //Shuffle for ordering
        if(evapMeltPocketList.size() >0) {
            EvapMeltPocket evapMeltPocket = evapMeltPocketList.get(1);
            int index = evapMeltPocketList.indexOf(evapMeltPocket);
            evapMeltPocketList.remove(index);
            evapMeltPocketList.add(0, evapMeltPocket);
        }
        return evapMeltPocketList;
    }
    
    private String getMeltIdSubString(Map<String, EvapMeltProperty> pocketToMeltIdMap, String pocketNumber) {
        if(pocketToMeltIdMap.get(pocketNumber) != null) {
            return pocketToMeltIdMap.get(pocketNumber).getState();
        } else {
            return null;
        }
    }
    
    public boolean ifMeltAlreadyInSystem(String meltId) {
        boolean isMeltAlreadyInSystem = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT COUNT(1) AS MELT_COUNT FROM EVAP_MELT_TRACKING WHERE MELT_ID=?";
        int meltCount=0;
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, meltId.trim());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                meltCount = rs.getInt("MELT_COUNT");
                if(meltCount > 0) {
                    isMeltAlreadyInSystem = true;
                }
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from EVAP_MELT_TRACKING inside ifMeltAlreadyInSystem: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return isMeltAlreadyInSystem;
    }
    
    public boolean isSafeLocationUpdationAllowed(String userName) {
        boolean isSafeLocationUpdationAllowed = false;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int userLevel=-1;
        String query = "SELECT USERLEVEL FROM USER_ACCESS_LEVEL WHERE USERNAME=?";
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, userName.trim());
            rs = pstmt.executeQuery();
            if(rs.next()) {
                userLevel = rs.getInt("USERLEVEL");
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from USER_ACCESS_LEVEL: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        if(userLevel > 2) {
            isSafeLocationUpdationAllowed = true;
        }
        return isSafeLocationUpdationAllowed;
    }
    
    public boolean ifMeltIsInAnotherToolOrPocket(String meltId, String eqpId, String pocketName, String userName) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String meltEqpId;
        String pocketNum;
        String qualStatus = "";
        String location = "";
        String query = "SELECT ep.EQPID, ep.POCKET_NAME, emt.QUALSTATUS, emt.LOCATION FROM EVAP_MELT_TRACKING emt LEFT OUTER JOIN EVAP_POCKETS ep ON ep.MELT_ID=emt.MELT_ID " +
            " WHERE emt.MELT_ID=?";
        if (pocketName.equalsIgnoreCase("3")) {
            pocketName = "AU3";
        }
        if (pocketName.equalsIgnoreCase("4")) {
            pocketName = "AU4";
        }
        boolean outputFlag = false;
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            pstmt.setString(1, meltId.trim());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                meltEqpId = rs.getString("EQPID");
                pocketNum = rs.getString("POCKET_NAME");
                if ((meltEqpId != null &&!meltEqpId.equalsIgnoreCase(eqpId)) || (pocketNum != null && !pocketNum.equalsIgnoreCase(pocketName))) {
                    outputFlag = true;
                } else {
                    qualStatus = rs.getString("QUALSTATUS");
                    if (!qualStatus.equalsIgnoreCase("QUALIFIED") && !qualStatus.equalsIgnoreCase("NOT_QUALIFIED")) {
                        outputFlag = true;
                    }
                    location = rs.getString("LOCATION");
                    if (location.equalsIgnoreCase("FAB_ENG_SAFE") && !isSafeLocationUpdationAllowed(userName)) {
                        outputFlag = true;
                    }
                }
            } else {
                outputFlag = true;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from EVAP_POCKETS: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return outputFlag;
    }
    
    private boolean ifMeltIdNull(List<EvapMeltPocket> evapMeltPocketList, Map<String, EvapMeltProperty> pocketToMeltIdMap, int index) {
        if(evapMeltPocketList.size() ==0) return false;
        return evapMeltPocketList.get(index).getMeltId() != null && pocketToMeltIdMap.get(evapMeltPocketList.get(index).getMeltId()) != null;
    }
    
    private Map<String, Boolean> getNoPrepMeltForEvapPocket(String evapId, Map<String, EvapMeltProperty> pocketToMeltIdMap) {
        Map<String, Boolean> noPrepMeltForEvapPocket = new HashMap<>();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer queryBuffer = new StringBuffer();
        queryBuffer.append("SELECT * from EVAP_POCKETS_NO_MELTST WHERE PREPID IN (");
        for(int listIndex=0; listIndex < pocketToMeltIdMap.size(); listIndex++) {
            queryBuffer.append(" ?");
            if(listIndex < pocketToMeltIdMap.size()-1) {
                queryBuffer.append(",");
            }
        }
        queryBuffer.append(") AND EQPID=?");
        int maintLoopIndex = 1;
        String pocketName;
        String state;
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(queryBuffer.toString());
            for (Map.Entry<String, EvapMeltProperty> entry : pocketToMeltIdMap.entrySet()) {
                pstmt.setString(maintLoopIndex, entry.getValue().getPrepId());
                maintLoopIndex++;
            }
            pstmt.setString(maintLoopIndex, evapId.trim());
            rs = pstmt.executeQuery();
            while(rs.next()) {
                pocketName = rs.getString("POCKET_NAME");
                state = rs.getString("STATE");
                noPrepMeltForEvapPocket.put(pocketName, state.equalsIgnoreCase("PREPPED") ? true : false);
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while populating inside getNoPrepMeltForEvapPocket(): "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return noPrepMeltForEvapPocket;
    }
    
    public List<MetalUsageObject> populateMetalUsageEntries(String evapId, Map<String, List<MetalUsageObject>> evapToDataMap) {
        List<EvapMeltPocket> evapMeltPocketList = getMeltPocketForEvap(evapId.trim());
        Map<String, EvapMeltProperty> pocketToMeltIdMap = fetchPocketInfo(evapId.trim());
        Map<String, Boolean> meltIdToprepStatus = ifAllowedToPrep(new ArrayList<String>(Arrays.asList(new String[] { ifMeltIdNull(evapMeltPocketList, pocketToMeltIdMap, 0) ? pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId() : "", 
            ifMeltIdNull(evapMeltPocketList, pocketToMeltIdMap, 1) ? pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId() : "", 
            ifMeltIdNull(evapMeltPocketList, pocketToMeltIdMap, 2) ? pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId() : "", 
            ifMeltIdNull(evapMeltPocketList, pocketToMeltIdMap, 3) ? pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId() : "", 
            ifMeltIdNull(evapMeltPocketList, pocketToMeltIdMap, 4) ? pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId() : "", 
            ifMeltIdNull(evapMeltPocketList, pocketToMeltIdMap, 5) ? pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId() : ""})));
        List<String> outLocationList = new ArrayList<>();
        List<MetalUsageObject> metalUsageObjectList = new ArrayList<>();
        List<MetalUsageObject> metalUsageObjList = evapToDataMap.get(evapId.trim());
        Map<String, Boolean> pocketToNoPrepStatus = getNoPrepMeltForEvapPocket(evapId, pocketToMeltIdMap);
        String meltWeightAU4=null; String meltWeightAU3=null; String meltWeightAUGE=null; String meltWeightNi=null; String meltWeightPt=null; String meltWeightTi=null;
        String preWeightAU4=null; String preWeightAU3=null; String preWeightAUGE=null; String preWeightNi=null; String preWeightPt=null; String preWeightTi=null;
        String largeWeightAU4=null; String largeWeightAU3=null; String largeWeightAUGE=null; String largeWeightNi=null; String largeWeightPt=null; String largeWeightTi=null;
        String smallWeightAU4=null; String smallWeightAU3=null; String smallWeightAUGE=null; String smallWeightNi=null; String smallWeightPt=null; String smallWeightTi=null;
        String postWeightAU4=null; String postWeightAU3=null; String postWeightAUGE=null; String postWeightNi=null; String postWeightPt=null; String postWeightTi=null;
        boolean isAu4PreppedWithNoMelt = pocketToNoPrepStatus.get("AU4") != null ? pocketToNoPrepStatus.get("AU4").booleanValue() : false; 
        boolean isAu3PreppedWithNoMelt = pocketToNoPrepStatus.get("AU3") != null ? pocketToNoPrepStatus.get("AU3").booleanValue() : false; 
        boolean isAugePreppedWithNoMelt = pocketToNoPrepStatus.get("AUGE") != null ? pocketToNoPrepStatus.get("AUGE").booleanValue() : false; 
        boolean isNiPreppedWithNoMelt = pocketToNoPrepStatus.get("NI") != null ? pocketToNoPrepStatus.get("NI").booleanValue() : false; 
        boolean isPtPreppedWithNoMelt = pocketToNoPrepStatus.get("PT") != null ? pocketToNoPrepStatus.get("PT").booleanValue() : false; 
        boolean isTiPreppedWithNoMelt = pocketToNoPrepStatus.get("TI") != null ? pocketToNoPrepStatus.get("TI").booleanValue() : false; 
        if (metalUsageObjectList != null) {
            for (MetalUsageObject metalUsageObject : metalUsageObjList) {
                if (metalUsageObject.getMetal() != null) {
                    if (metalUsageObject.getMetal().equalsIgnoreCase("AU4")) {
                        meltWeightAU4 = metalUsageObject.getMeltWeight();
                        preWeightAU4 = metalUsageObject.getPreAdd();
                        largeWeightAU4 = metalUsageObject.getQtyLgPellet();
                        smallWeightAU4 = metalUsageObject.getQtySmPellet();
                        postWeightAU4 = metalUsageObject.getPostAdd();
                    } else if (metalUsageObject.getMetal().equalsIgnoreCase("AU3")) {
                        meltWeightAU3 = metalUsageObject.getMeltWeight();
                        preWeightAU3 = metalUsageObject.getPreAdd();
                        largeWeightAU3 = metalUsageObject.getQtyLgPellet();
                        smallWeightAU3 = metalUsageObject.getQtySmPellet();
                        postWeightAU3 = metalUsageObject.getPostAdd();
                    } else if (metalUsageObject.getMetal().equalsIgnoreCase("AUGE")) {
                        meltWeightAUGE = metalUsageObject.getMeltWeight();
                        preWeightAUGE = metalUsageObject.getPreAdd();
                        largeWeightAUGE = metalUsageObject.getQtyLgPellet();
                        smallWeightAUGE = metalUsageObject.getQtySmPellet();
                        postWeightAUGE = metalUsageObject.getPostAdd();
                    } else if (metalUsageObject.getMetal().equalsIgnoreCase("NI")) {
                        meltWeightNi = metalUsageObject.getMeltWeight();
                        preWeightNi = metalUsageObject.getPreAdd();
                        largeWeightNi = metalUsageObject.getQtyLgPellet();
                        smallWeightNi = metalUsageObject.getQtySmPellet();
                        postWeightNi = metalUsageObject.getPostAdd();
                    } else if (metalUsageObject.getMetal().equalsIgnoreCase("PT")) {
                        meltWeightPt = metalUsageObject.getMeltWeight();
                        preWeightPt = metalUsageObject.getPreAdd();
                        largeWeightPt = metalUsageObject.getQtyLgPellet();
                        smallWeightPt = metalUsageObject.getQtySmPellet();
                        postWeightPt = metalUsageObject.getPostAdd();
                    } else if (metalUsageObject.getMetal().equalsIgnoreCase("TI")) {
                        meltWeightTi = metalUsageObject.getMeltWeight();
                        preWeightTi = metalUsageObject.getPreAdd();
                        largeWeightTi = metalUsageObject.getQtyLgPellet();
                        smallWeightTi = metalUsageObject.getQtySmPellet();
                        postWeightTi = metalUsageObject.getPostAdd();
                    }
                }
            }
        }
        
        metalUsageObjectList.add(new MetalUsageObject(false, false, false, pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(0).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId() : "", meltWeightAU4 != null ? meltWeightAU4 : "", pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(0).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId() : "", evapId, preWeightAU4 != null ? preWeightAU4 : pocketToMeltIdMap.get("AU4") != null && pocketToMeltIdMap.get("AU4").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU4").getBaseWeight() : "", postWeightAU4 != null ? postWeightAU4 : pocketToMeltIdMap.get("AU4") != null && pocketToMeltIdMap.get("AU4").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU4").getPostWeight(): "", largeWeightAU4 != null ? largeWeightAU4 : pocketToMeltIdMap.get("AU4") != null && pocketToMeltIdMap.get("AU4").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU4").getMainAddWeight() : "", smallWeightAU4 != null ? smallWeightAU4 : pocketToMeltIdMap.get("AU4") != null && pocketToMeltIdMap.get("AU4").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU4").getSubAddWeight() : "", evapMeltPocketList.get(0).getMeltId(), evapMeltPocketList.get(0).getPocketNumber(), "Lg Pellet", "Sm Pellet", pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()) == null ? false : pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(0).getMeltId()) ? false : true, getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(0).getMeltId()) != null ? getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(0).getMeltId()) : "", pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()) != null  && meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId()) != null ? meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(0).getMeltId()).getMeltId()) : false, outLocationList, isAu4PreppedWithNoMelt)); 
        metalUsageObjectList.add(new MetalUsageObject(false, false, false, pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(1).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId() : "", meltWeightAU3 != null ? meltWeightAU3 : "", pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(1).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId() : "", evapId, preWeightAU3 != null ? preWeightAU3 : pocketToMeltIdMap.get("AU3") != null && pocketToMeltIdMap.get("AU3").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU3").getBaseWeight() : "", postWeightAU3 != null ? postWeightAU3 : pocketToMeltIdMap.get("AU3") != null && pocketToMeltIdMap.get("AU3").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU3").getPostWeight(): "", largeWeightAU3 != null ? largeWeightAU3 : pocketToMeltIdMap.get("AU3") != null && pocketToMeltIdMap.get("AU3").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU3").getMainAddWeight() : "", smallWeightAU3 != null ? smallWeightAU3 : pocketToMeltIdMap.get("AU3") != null && pocketToMeltIdMap.get("AU3").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AU3").getSubAddWeight() : "", evapMeltPocketList.get(1).getMeltId(), evapMeltPocketList.get(1).getPocketNumber(), "Lg Pellet", "Sm Pellet", pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()) == null ? false : pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(1).getMeltId()) ? false : true, getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(1).getMeltId()) != null ? getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(1).getMeltId()) : "", pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()) != null && meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId()) != null ? meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(1).getMeltId()).getMeltId()) : false, outLocationList, isAu3PreppedWithNoMelt));
        metalUsageObjectList.add(new MetalUsageObject(false, false, false, pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(2).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId() : "", meltWeightAUGE != null ? meltWeightAUGE : "", pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(2).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId() : "", evapId, preWeightAUGE != null ? preWeightAUGE : pocketToMeltIdMap.get("AUGE") != null && pocketToMeltIdMap.get("AUGE").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AUGE").getBaseWeight() : "", postWeightAUGE != null ? postWeightAUGE : pocketToMeltIdMap.get("AUGE") != null && pocketToMeltIdMap.get("AUGE").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AUGE").getPostWeight(): "", largeWeightAUGE != null ? largeWeightAUGE : pocketToMeltIdMap.get("AUGE") != null && pocketToMeltIdMap.get("AUGE").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AUGE").getMainAddWeight() : "", smallWeightAUGE != null ? smallWeightAUGE : pocketToMeltIdMap.get("AUGE") != null && pocketToMeltIdMap.get("AUGE").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("AUGE").getSubAddWeight() : "", evapMeltPocketList.get(2).getMeltId(), evapMeltPocketList.get(2).getPocketNumber(), "AuGe", "Ta", pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()) == null ? false : pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(2).getMeltId()) ? false : true, getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(2).getMeltId()) != null ? getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(2).getMeltId()) : "", pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()) != null && meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId()) != null ? meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(2).getMeltId()).getMeltId()) : false, outLocationList, isAugePreppedWithNoMelt));
        metalUsageObjectList.add(new MetalUsageObject(false, false, false, pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(3).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId() : "", meltWeightNi != null ? meltWeightNi : "", pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(3).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId() : "", evapId, preWeightNi != null ? preWeightNi : pocketToMeltIdMap.get("NI") != null && pocketToMeltIdMap.get("NI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("NI").getBaseWeight() : "", postWeightNi != null ? postWeightNi : pocketToMeltIdMap.get("NI") != null && pocketToMeltIdMap.get("NI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("NI").getPostWeight(): "", largeWeightNi != null ? largeWeightNi : pocketToMeltIdMap.get("NI") != null && pocketToMeltIdMap.get("NI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("NI").getMainAddWeight() : "", smallWeightNi != null ? smallWeightNi : pocketToMeltIdMap.get("NI") != null && pocketToMeltIdMap.get("NI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("NI").getSubAddWeight() : "", evapMeltPocketList.get(3).getMeltId(), evapMeltPocketList.get(3).getPocketNumber(), "Ni", "", pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()) == null ? false : pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(3).getMeltId()) ? false : true, getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(3).getMeltId()) != null ? getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(3).getMeltId()) : "", pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()) != null && meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId()) != null ? meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(3).getMeltId()).getMeltId()) : false, outLocationList, isNiPreppedWithNoMelt));
        metalUsageObjectList.add(new MetalUsageObject(false, false, false, pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(4).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId() : "", meltWeightPt != null ? meltWeightPt : "", pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(4).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId() : "", evapId, preWeightPt != null ? preWeightPt : pocketToMeltIdMap.get("PT") != null && pocketToMeltIdMap.get("PT").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("PT").getBaseWeight() : "", postWeightPt != null ? postWeightPt : pocketToMeltIdMap.get("PT") != null && pocketToMeltIdMap.get("PT").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("PT").getPostWeight(): "", largeWeightPt != null ? largeWeightPt : pocketToMeltIdMap.get("PT") != null && pocketToMeltIdMap.get("PT").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("PT").getMainAddWeight() : "", smallWeightPt != null ? smallWeightPt : pocketToMeltIdMap.get("PT") != null && pocketToMeltIdMap.get("PT").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("PT").getSubAddWeight() : "", evapMeltPocketList.get(4).getMeltId(), evapMeltPocketList.get(4).getPocketNumber(), "Pt", "", pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()) == null ? false : pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(4).getMeltId()) ? false : true, getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(4).getMeltId()) != null ? getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(4).getMeltId()) : "", pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()) != null && meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId()) != null ? meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(4).getMeltId()).getMeltId()) : false, outLocationList, isPtPreppedWithNoMelt));
        metalUsageObjectList.add(new MetalUsageObject(false, false, false, pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(5).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId() : "", meltWeightTi != null ? meltWeightTi : "", pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()) != null && !pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(5).getMeltId()) ? pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId() : "", evapId, preWeightTi != null ? preWeightTi : pocketToMeltIdMap.get("TI") != null && pocketToMeltIdMap.get("TI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("TI").getBaseWeight() : "", postWeightTi != null ? postWeightTi : pocketToMeltIdMap.get("TI") != null && pocketToMeltIdMap.get("TI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("TI").getPostWeight(): "", largeWeightTi != null ? largeWeightTi : pocketToMeltIdMap.get("TI") != null && pocketToMeltIdMap.get("TI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("TI").getMainAddWeight() : "", smallWeightTi != null ? smallWeightTi : pocketToMeltIdMap.get("TI") != null && pocketToMeltIdMap.get("TI").getState().equalsIgnoreCase("PREPPED") ? pocketToMeltIdMap.get("TI").getSubAddWeight() : "", evapMeltPocketList.get(5).getMeltId(), evapMeltPocketList.get(5).getPocketNumber(), "Ti", "", pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()) == null ? false : pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId().contains("_" + evapMeltPocketList.get(5).getMeltId()) ? false : true, getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(5).getMeltId()) != null ? getMeltIdSubString(pocketToMeltIdMap, evapMeltPocketList.get(5).getMeltId()) : "", pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()) != null && meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId()) != null ? meltIdToprepStatus.get(pocketToMeltIdMap.get(evapMeltPocketList.get(5).getMeltId()).getMeltId()) : false, outLocationList, isTiPreppedWithNoMelt));
        return metalUsageObjectList;
    }
    
    public List<String> populateLocation() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT DISTINCT(LOCATION) FROM EVAP_MELT_TRACKING order by LOCATION";
        List<String> outLocationData = new ArrayList<>();
        
        try {
            conn = open();
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
            while(rs.next()) {
                outLocationData.add(rs.getString("LOCATION"));
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        } catch(SQLException sqle) {
            System.err.println("Sql exception while selecting from populateLocation: "+sqle.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
                rs = null;
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                }
                pstmt = null;
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
                conn = null;
            }
        }
        return outLocationData;
    }
}
