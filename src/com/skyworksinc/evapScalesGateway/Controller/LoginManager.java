package com.skyworksinc.evapScalesGateway.Controller;

import com.skyworksinc.cim.promis.transactions.ConfigurationServer;
import com.skyworksinc.cim.promis.transactions.Credentials;
import java.io.IOException;
import java.util.logging.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import com.skyworksinc.cim.promis.transactions.TransactionServiceException;

/**
 * Manages control flow for logins
 */
public class LoginManager {

    private Scene scene;
 
    public LoginManager(Scene scene) {
        this.scene = scene;
    }

    /**
     * Callback method invoked to notify that a user has been authenticated.
     * Will show the main application screen.
     */
    public void authenticated(String loggedInUser) {
        showMainView(loggedInUser);
    }

    public void showMainView(String loggedInUser) {
        try {
            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource("mainview.fxml")
            );
            scene.setRoot((Parent) loader.load());
            MainViewController controller
                    = loader.<MainViewController>getController();
            controller.initSessionID(loggedInUser);
        } catch (IOException ex) {
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
