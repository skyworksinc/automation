package com.skyworksinc.evapScalesGateway.Controller;

import java.io.IOException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import com.skyworksinc.evapScalesGateway.Controller.LoginManager;
import javafx.stage.Screen;
import javafx.geometry.Rectangle2D;
import javafx.scene.paint.Color;
import javafx.stage.StageStyle;

/**
 *
 * @author mattoop
 */
public class EvapScalesGatewayMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
       Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
       Scene scene = new Scene(new StackPane(), visualBounds.getWidth(), visualBounds.getHeight(), Color.WHITE);

        stage.setTitle("Metal Usage");
        stage.setScene(scene);
        scene.getStylesheets().addAll(EvapScalesGatewayMain.class.getResource("myStyles.css").toExternalForm());
        
        LoginManager loginManager = new LoginManager(scene);
        loginManager.showMainView("NOT LOGGED IN");
        //loginManager.showLoginScreen();
        stage.show();
    }

}
