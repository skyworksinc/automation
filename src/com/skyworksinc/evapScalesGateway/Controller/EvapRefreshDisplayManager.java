package com.skyworksinc.evapScalesGateway.Controller;

import com.skyworksinc.evapScalesGateway.beans.MetalUsageObject;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import javafx.animation.Animation;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.util.Duration;
import javafx.animation.Timeline;
import javafx.animation.KeyFrame;

/**
 *
 * @author mattoop
 */
public class EvapRefreshDisplayManager {

    private static Map<String, Timeline> evapToRefreshTransition = new HashMap<>();
    private Map<String, List<MetalUsageObject>> evapToDataMap = null;
    private ObservableList<MetalUsageObject> data;
    private ObservableList<String> outLocationValues;
    private Label prepStatus;
    private TableView<MetalUsageObject> tableView;

    public void createEvapRefreshDisplayTransition(String evapId) {
        MainViewController mainViewController = new MainViewController();
        Timeline refreshDisplay = new Timeline(new KeyFrame(
                Duration.minutes(30), 
                event -> {
                    mainViewController.populateTableForRefresh(evapId.trim(), evapToDataMap, data, outLocationValues, prepStatus, tableView);
                }
        )
        );
        evapToRefreshTransition.put(evapId, refreshDisplay);
        refreshDisplay.setCycleCount(Animation.INDEFINITE);
    }

    public void scheduleForRefresh(String evapId, Map<String, List<MetalUsageObject>> evapToDataMap, ObservableList<MetalUsageObject> data, ObservableList<String> outLocationValues,
            Label prepStatus, TableView<MetalUsageObject> tableView) {
        evapToRefreshTransition.get(evapId).play();
        setEvapToDataMap(evapToDataMap);
        setData(data);
        setOutLocationValues(outLocationValues);
        setPrepStatus(prepStatus);
        setTableView(tableView);
    }

    /**
     * @param evapToDataMap the evapToDataMap to set
     */
    public void setEvapToDataMap(Map<String, List<MetalUsageObject>> evapToDataMap) {
        this.evapToDataMap = evapToDataMap;
    }

    /**
     * @param data the data to set
     */
    public void setData(ObservableList<MetalUsageObject> data) {
        this.data = data;
    }

    /**
     * @param outLocationValues the outLocationValues to set
     */
    public void setOutLocationValues(ObservableList<String> outLocationValues) {
        this.outLocationValues = outLocationValues;
    }

    /**
     * @param prepStatus the prepStatus to set
     */
    public void setPrepStatus(Label prepStatus) {
        this.prepStatus = prepStatus;
    }

    /**
     * @param tableView the tableView to set
     */
    public void setTableView(TableView<MetalUsageObject> tableView) {
        this.tableView = tableView;
    }

}
