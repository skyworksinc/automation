package com.skyworksinc.evapScalesGateway.Controller;

import com.skyworksinc.evapScalesGateway.beans.MetalUsageObject;
import javafx.animation.PauseTransition;
import java.util.Map;
import java.util.HashMap;
import javafx.util.Duration;
import java.util.List;

/**
 *
 * @author mattoop
 */
public class EvapPauseTransitionManager {
    private static Map<String, PauseTransition> evapToPauseTransition = new HashMap<>(); 
    private Map<String, List<MetalUsageObject>> evapToDataMap = null;
    
    public void createEvapPauseTransition(String evapId) {
        MainViewController mainViewController = new MainViewController();
        PauseTransition resetCredentials = new PauseTransition(Duration.minutes(20));
        resetCredentials.setOnFinished(e -> mainViewController.expireCredentialsForEvap(evapId.trim(), evapToDataMap));
        evapToPauseTransition.put(evapId, resetCredentials);
    }
    
    public void scheduleLogin(String evapId, Map<String, List<MetalUsageObject>> evapToDataMap) {
        evapToPauseTransition.get(evapId).playFromStart();
        setEvapToDataMap(evapToDataMap);
    }

    /**
     * @param evapToDataMap the evapToDataMap to set
     */
    public void setEvapToDataMap(Map<String, List<MetalUsageObject>> evapToDataMap) {
        this.evapToDataMap = evapToDataMap;
    }
    
}
