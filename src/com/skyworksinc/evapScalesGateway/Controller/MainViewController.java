package com.skyworksinc.evapScalesGateway.Controller;

import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TableView;
import com.skyworksinc.evapScalesGateway.beans.MetalUsageObject;
import java.util.Map;
import java.util.List;
import javafx.collections.ObservableList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import java.net.URL;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import javafx.scene.control.TextArea;
import com.skyworksinc.evapScalesGateway.dao.EvapScalesGatewayDao;
import com.skyworksinc.evapScalesGateway.dialogs.InMeltIdValidationDialogController;
import com.skyworksinc.evapScalesGateway.dialogs.UserAccessLevelDialogController;
import com.skyworksinc.evapScalesGateway.dialogs.ErrorPrepPocketController;
import java.util.Iterator;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.util.converter.DefaultStringConverter;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.collections.ListChangeListener;
import javafx.beans.Observable;
import java.io.IOException;
import com.skyworksinc.evapScalesGateway.dialogs.OutMeltIdValidationDialogController;
import com.skyworksinc.evapScalesGateway.dialogs.MeltIdValidationDialogController;
import com.skyworksinc.evapScalesGateway.dialogs.SmallPelletDialogController;
import com.fazecast.jSerialComm.SerialPort;
import com.skyworksinc.cim.promis.transactions.ConfigurationServer;
import com.skyworksinc.cim.promis.transactions.Credentials;
import com.skyworksinc.evapScalesGateway.beans.PrepPocketStatusMessage;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.HashMap;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import java.util.UUID;
import java.util.Arrays;
import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controls the main application screen
 */
public class MainViewController implements Initializable {
    
    @FXML
    private TableView<MetalUsageObject> tableView;
    @FXML
    private TableColumn<MetalUsageObject, Boolean> tableColumnMakeMelt;
    @FXML
    private TableColumn<MetalUsageObject, Boolean> tableColumnSwapMelt;
    @FXML
    private TableColumn<MetalUsageObject, Boolean> tableColumnNoMelt;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnOutMeltId;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnMeltWeight;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnInMeltId;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnInLocation;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnOutLocation; 
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnPreAdd;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnQtyLgPellet;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnQtySmPellet;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnMetal;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnPocket;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnMetalLgPlt;
    @FXML
    private TableColumn<MetalUsageObject, String> tableColumnMetalSmPlt;
    @FXML
    private TableColumn<MetalUsageObject, Boolean> cancelPrep;
    @FXML
    private TableColumn<MetalUsageObject, Boolean> prepTime;
    //@FXML
    //private Button logoutButton;
    @FXML
    private Button n_evap06;
    @FXML
    private Button n_evap07;
    @FXML
    private Button n_evap08;
    @FXML
    private Button n_evap09;
    @FXML
    private Button n_evap10;
    @FXML
    private Button n_evap11;
    @FXML
    private Button n_evap12;
    @FXML
    private Button n_evap13;
    @FXML
    private Button n_evap14;
    @FXML
    private Button n_evap15;
    @FXML
    private Button n_evap16;
    @FXML
    private Button n_evap17;
    @FXML
    private Button n_evap18;
    @FXML
    private Button n_evap19;
    @FXML
    private Button submitButton;
    @FXML
    protected Label usernameLabel;
    @FXML
    private Label prepStatus;
    EvapScalesGatewayDao evapScalesGatewayDao = null;
    private SerialPort device = null;
    boolean isScalesConnected = false;
    private OutputStream outputStream = null;
    private BufferedReader inputStream = null;
    String comPortName = "Prolific USB-to-Serial Comm Port ";
    private String currentEvapId = null;
    private Map<String, List<MetalUsageObject>> evapToDataMap = null;
    private Scene scene;
    //Set<String> evapIdsForTimerTask = new LinkedHashSet<>();
    EvapPauseTransitionManager evapPauseTransitionManager = null;
    EvapRefreshDisplayManager evapRefreshDisplayManager = null;
    private String backUpInputMeltId;
    
    /**
     * @param evapToDataMap the evapToDataMap to set
     */
    public void setEvapToDataMap(Map<String, List<MetalUsageObject>> evapToDataMap) {
        this.evapToDataMap = evapToDataMap;
    }

    /**
     * @param backUpInputMeltId the backUpInputMeltId to set
     */
    public void setBackUpInputMeltId(String backUpInputMeltId) {
        this.backUpInputMeltId = backUpInputMeltId;
    }

    /**
     * @param data the data to set
     */
    public void setData(ObservableList<MetalUsageObject> data) {
        this.data = data;
    }

    /**
     * @param outLocationValues the outLocationValues to set
     */
    public void setOutLocationValues(ObservableList<String> outLocationValues) {
        this.outLocationValues = outLocationValues;
    }

    /**
     * @param prepStatus the prepStatus to set
     */
    public void setPrepStatus(Label prepStatus) {
        this.prepStatus = prepStatus;
    }

    /**
     * @param tableView the tableView to set
     */
    public void setTableView(TableView<MetalUsageObject> tableView) {
        this.tableView = tableView;
    }
    
    public enum Evaps {
        
        N_EVAP06("N_EVAP06"),
        N_EVAP07("N_EVAP07"),
        N_EVAP08("N_EVAP08"),
        N_EVAP09("N_EVAP09"),
        N_EVAP10("N_EVAP10"),
        N_EVAP11("N_EVAP11"),
        N_EVAP12("N_EVAP12"),
        N_EVAP13("N_EVAP13"),
        N_EVAP14("N_EVAP14"),
        N_EVAP15("N_EVAP15"),
        N_EVAP16("N_EVAP16"),
        N_EVAP17("N_EVAP17"),
        N_EVAP18("N_EVAP18"),
        N_EVAP19("N_EVAP19");
        
        private final String evapText;
        
        Evaps(final String evapText) {
            this.evapText = evapText;
        }
        
        @Override
        public String toString() {
            return evapText;
        }
    }

    /**
     * This is the amount of time for which a login is valid. This time should
     * be long enough that the operator can load the tool without being
     * pressured into making mistakes, but not so long that the login can be
     * used for another purpose.
     */
    public static final int ALLOWABLE_LOGIN_TIME = 20 * 60 * 1000;
    
    private int matchScaleComPort(SerialPort[] serialPortArray) {
        String out = "";
        for (int serialPortArrayIndex = 0; serialPortArrayIndex < serialPortArray.length; serialPortArrayIndex++) {
            String tmp = "";
            tmp = serialPortArray[serialPortArrayIndex].getDescriptivePortName();
            char[] remover = tmp.toCharArray();
            tmp = "";
            for (char each : remover) {
                if (each != '(') {
                    tmp += each;
                } else {
                    break;
                }
            }
            out += tmp;
            out += "\n";
            if (tmp.equals(comPortName)) {
                return serialPortArrayIndex;
            }
        }
        return -1;
    }
    
    public void showMainView(String loggedInUser) {
        try {
            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource("mainview.fxml")
            );
            scene.setRoot((Parent) loader.load());
            MainViewController controller
                    = loader.<MainViewController>getController();
            controller.initSessionID(loggedInUser);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(LoginManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    private ObservableList<MetalUsageObject> data;
    private ObservableList<String> outLocationValues;
    
    List<MetalUsageObject> metalUsageObjectItem;
    
    public void populateTableForRefresh(String evapId, Map<String, List<MetalUsageObject>> evapToDataLocalMap, ObservableList<MetalUsageObject> data, ObservableList<String> outLocationValues, Label prepStatusLabelLocal,
            TableView<MetalUsageObject> tableView) {
        if (evapToDataLocalMap != null) {
            setEvapToDataMap(evapToDataLocalMap);
        }
        if (data != null) {
            setData(data);
        }
        if (outLocationValues != null) {
            setOutLocationValues(outLocationValues);
        }
        if (prepStatusLabelLocal != null) {
            setPrepStatus(prepStatusLabelLocal);
        }
        if (tableView != null) {
            setTableView(tableView);
        }
        populateTable(evapId);
    }
    
    public void populateTable(String evapId) {
        evapScalesGatewayDao = new EvapScalesGatewayDao();
        metalUsageObjectItem = evapScalesGatewayDao.populateMetalUsageEntries(evapId.trim(), evapToDataMap);
        List<String> outLocationList = evapScalesGatewayDao.populateLocation();
        
        if (data != null) {
            data.removeAll(data);
        }
        
        Iterator<MetalUsageObject> metalUsageObjectIterator = metalUsageObjectItem.iterator();
        while (metalUsageObjectIterator.hasNext()) {
            MetalUsageObject metalUsageObj = metalUsageObjectIterator.next();
            data.add(metalUsageObj);
        }
        
        if (outLocationValues != null) {
            outLocationValues.removeAll(outLocationValues);
        }
        
        Iterator<String> outLocationListIterator = outLocationList.iterator();
        while (outLocationListIterator.hasNext()) {
            outLocationValues.add(outLocationListIterator.next());
        }
        // set the currentEvapId
        setEvapId(evapId);

        //Set the prep status
        boolean isPartiallyPrepped = false;
        boolean notFullyPrepped = false;
        Iterator<MetalUsageObject> metalUsjObjIterator = data.iterator();
        while (metalUsjObjIterator.hasNext()) {
            MetalUsageObject metalUsageObject = metalUsjObjIterator.next();
            if (metalUsageObject.getMeltState().equalsIgnoreCase("PREPPED")
                    || metalUsageObject.getIsPreppedWithNoMelt()) {
                isPartiallyPrepped = true;
                if (notFullyPrepped == true) {
                    break;
                }
            } else if (!metalUsageObject.getMeltState().equalsIgnoreCase("PREPPED") && !metalUsageObject.getPocket().equalsIgnoreCase("0")) {
                if (!metalUsageObject.getIsPreppedWithNoMelt()) {
                    notFullyPrepped = true;
                } else {
                    notFullyPrepped = false;
                }
            }
        }
        if (notFullyPrepped == false) {
            prepStatus.setText("PREPPED");
            prepStatus.setTextFill(Color.GREEN);
        } else if (isPartiallyPrepped == true) {
            prepStatus.setText("PARTIALLY PREPPED");
            prepStatus.setTextFill(Color.YELLOW);
        } else {
            prepStatus.setText("NOT PREPPED");
            prepStatus.setTextFill(Color.RED);
        }
        
        tableView.setItems(null);
        tableView.layout();
        tableView.setItems(FXCollections.observableList(data));
        tableView.refresh();
        
    }
    
    public static <T> void refresh(final TableView<T> table, final List<T> tableList) {
        //Wierd JavaFX bug 
        table.setItems(null);
        table.layout();
        table.setItems(FXCollections.observableList(tableList));
    }
    
    private boolean openScalesConnection() {
        SerialPort[] serialPorts;
        serialPorts = SerialPort.getCommPorts();
        //Initialize Scales connection
        int scaleSerialPort = matchScaleComPort(serialPorts);
        if (scaleSerialPort != -1) {
            device = serialPorts[scaleSerialPort];
            isScalesConnected = device.openPort();
            if (isScalesConnected) {
                device.setComPortParameters(9600, 7, SerialPort.ONE_STOP_BIT, SerialPort.EVEN_PARITY);
                outputStream = device.getOutputStream();
                inputStream = new BufferedReader(new InputStreamReader(device.getInputStream()));
            }
            return isScalesConnected;
        }
        return false;
    }
    
    private String readScalesMesaurement(boolean pre, boolean lgPlt, boolean post) {
        
        if (post) {
            return "130";
        }
        if (pre) {
            return "140.0";
        }
        if (lgPlt) {
            return "140";
        }
        String retu = "";
        int trim = 4;
        
        String regexDecimal = "^-?\\d*\\.\\d+$";
        String regexInteger = "^-?\\d+$";
        String regexDouble = regexDecimal + "|" + regexInteger;
        Pattern p = Pattern.compile("[+-]?\\d*\\.?\\d+");
        
        while (device.bytesAvailable() > 0) {
            String temp = null;
            try {
                temp = inputStream.readLine();
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(MainViewController.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            
            if (temp != null) {
                Matcher m = p.matcher(temp);
                if (m.find()) {
                    if (m.group().charAt(0) == '+' || m.group().charAt(0) == '-') {
                        retu = m.group().substring(1);
                    }
                }
            }
        }
        
        return retu;
    }
    
    @FXML
    public void initialize(URL url, ResourceBundle rb) {
        //Providing callback to the checkboxes
        setData(FXCollections.observableArrayList(new Callback<MetalUsageObject, Observable[]>() {
            
            @Override
            public Observable[] call(MetalUsageObject param) {
                return new Observable[]{param.makeMeltProperty(), param.swapMeltProperty(), param.noMeltProperty()};
            }
        }));
        
        setOutLocationValues(FXCollections.observableArrayList());
        Logger.getLogger(MainViewController.class.getName()).log(Level.INFO, "Started Logging...");
        
        tableColumnMakeMelt.setEditable(true);
        tableColumnSwapMelt.setEditable(true);
        tableColumnNoMelt.setEditable(true);
        tableView.setEditable(true);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //Disable other Evaps
        /*
         n_evap06.setDisable(true);      n_evap07.setDisable(true);
         n_evap08.setDisable(true);      n_evap09.setDisable(true);
         n_evap10.setDisable(true);      n_evap11.setDisable(true);
         n_evap12.setDisable(true);      n_evap13.setDisable(true);
         n_evap14.setDisable(true);      n_evap15.setDisable(true);
         n_evap16.setDisable(true);      n_evap17.setDisable(true);
         n_evap18.setDisable(true);
         */
        if (evapPauseTransitionManager == null) {
            evapPauseTransitionManager = new EvapPauseTransitionManager();
            for (Evaps evap : Evaps.values()) {
                evapPauseTransitionManager.createEvapPauseTransition(evap.toString());
            }
        }
        
        if (evapRefreshDisplayManager == null) {
            evapRefreshDisplayManager = new EvapRefreshDisplayManager();
            for (Evaps evap : Evaps.values()) {
                evapRefreshDisplayManager.createEvapRefreshDisplayTransition(evap.toString());
            }
        }

        //If logout, clear data only for current Evap
        if (this.evapToDataMap != null && this.evapToDataMap.size() > 0) {
            this.evapToDataMap.remove(currentEvapId);
            setEvapToDataMap(this.evapToDataMap);
        } else {
            setEvapToDataMap(new HashMap<>());
        }

        //Open Scales communication
        if (isScalesConnected == false) {
             openScalesConnection();
        }
        
        data.addListener(new ListChangeListener<MetalUsageObject>() {
            
            @Override
            public void onChanged(ListChangeListener.Change<? extends MetalUsageObject> c) {
                while (c.next()) {
                    if (c.wasUpdated()) {
                        tableView.setItems(null);
                        tableView.layout();
                        //Clearing prior Melt weight entries
                        if (metalUsageObjectItem.get(c.getFrom()).getMakeMelt() == false || metalUsageObjectItem.get(c.getFrom()).getSwapMelt() == false
                                || metalUsageObjectItem.get(c.getFrom()).getNoMelt() == false) {
                            setBackUpInputMeltId(metalUsageObjectItem.get(c.getFrom()).getInMeltId());
                            metalUsageObjectItem.get(c.getFrom()).setMeltWeight("");
                            metalUsageObjectItem.get(c.getFrom()).setInMeltId("");
                            metalUsageObjectItem.get(c.getFrom()).setOutMeltId("");
                            metalUsageObjectItem.get(c.getFrom()).setPreAdd("");
                            metalUsageObjectItem.get(c.getFrom()).setQtyLgPellet("");
                            metalUsageObjectItem.get(c.getFrom()).setQtySmPellet("");
                            metalUsageObjectItem.get(c.getFrom()).setPostAdd("");
                        }
                        if (metalUsageObjectItem.get(c.getFrom()).getNoMelt() == false && metalUsageObjectItem.get(c.getFrom()).getMetal().equalsIgnoreCase("AU4")) {
                            if (metalUsageObjectItem.get(1).getNoMelt() == false) {
                                tableView.setItems(FXCollections.observableList(data));
                            } else {
                                metalUsageObjectItem.get(0).setNoMelt(true);
                            }
                        }
                        // System.out.println("Value " + metalUsageObjectItem.get(c.getFrom()).getNoMelt() + " changed to " + metalUsageObjectItem.get(c.getFrom()).getMetal());
                        tableView.setItems(FXCollections.observableList(data));
                    }
                }
                
                tableView.setItems(null);
                tableView.layout();
                tableView.setItems(FXCollections.observableList(data));
                tableView.refresh();
            }
        });
        
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tableColumnMakeMelt.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, Boolean>("makeMelt")
                );
                tableColumnMakeMelt.setCellFactory(column -> {
                    return new CheckBoxTableCell<MetalUsageObject, Boolean>() {
                        @Override
                        public void updateItem(Boolean item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            TableRow<MetalUsageObject> currentRow = getTableRow();
                            if (currentRow.getItem() != null && !empty) {
                                if (currentRow.getItem().getMeltState().equalsIgnoreCase("PREPPED")
                                        || currentRow.getItem().getIsPreppedWithNoMelt()) {
                                    setDisable(true);
                                    setEditable(false);
                                    this.setStyle("-fx-background-color: green");
                                } else if (currentRow.getItem().getPocket().equalsIgnoreCase("0")) {
                                    setDisable(true);
                                    setEditable(false);
                                    this.setStyle("-fx-background-color: lightgrey");
                                }
                                if (currentRow.getItem().getSwapMelt() == true || currentRow.getItem().getNoMelt() == true) {
                                    currentRow.getItem().setMakeMelt(false);
                                    this.setDisable(true);
                                    setStyle(tableView.getStyle());
                                }
                            } else {
                                setStyle(tableView.getStyle());
                            }
                        }
                    };
                });

                /*
                 tableColumnMakeMelt.setCellFactory(new Callback<TableColumn<MetalUsageObject, Boolean>, TableCell<MetalUsageObject, Boolean>>() {
                 @Override
                 public TableCell<MetalUsageObject, Boolean> call(TableColumn<MetalUsageObject, Boolean> param) {
                 return new CheckBoxTableCell<MetalUsageObject, Boolean>() {
                 {
                 setAlignment(Pos.CENTER);
                 }

                 @Override
                 public void updateItem(Boolean item, boolean empty) {
                 super.updateItem(item, empty);
                 if (!empty) {
                 TableRow row = getTableRow();

                 if (row != null) {
                 int rowNo = row.getIndex();
                 TableView.TableViewSelectionModel sm = getTableView().getSelectionModel();

                 if (item) {
                 MetalUsageObject metalUsageObjItem = getTableView().getItems().get(getIndex());
                 if(metalUsageObjItem.getSwapMelt() == true || metalUsageObjItem.getNoMelt() == true) {
                 setDisable(true);
                 setEditable(false);
                 //this.setStyle("-fx-text-fill: black");
                 setStyle(tableView.getStyle());
                 //sm.clearSelection(rowNo);
                 //empty = true;
                 } else {
                 sm.select(rowNo);
                 }
                 } else {
                 sm.clearSelection(rowNo);
                 }
                 }
                 }

                                 
                 }
                 };
                 }
                 });*/
                tableColumnSwapMelt.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, Boolean>("swapMelt")
                );
                tableColumnSwapMelt.setCellFactory(column -> {
                    return new CheckBoxTableCell<MetalUsageObject, Boolean>() {
                        @Override
                        public void updateItem(Boolean item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            TableRow<MetalUsageObject> currentRow = getTableRow();
                            if (currentRow.getItem() != null && !empty) {
                                if (currentRow.getItem().getMeltState().equalsIgnoreCase("PREPPED") || currentRow.getItem().getIsPreppedWithNoMelt()) {
                                    setDisable(true);
                                    setEditable(false);
                                    this.setStyle("-fx-background-color: green");
                                } else if (currentRow.getItem().getPocket().equalsIgnoreCase("0")) {
                                    setDisable(true);
                                    setEditable(false);
                                    this.setStyle("-fx-background-color: lightgrey");
                                }
                                if (currentRow.getItem().getMakeMelt() == true || currentRow.getItem().getNoMelt() == true) {
                                    currentRow.getItem().setSwapMelt(false);
                                    this.setDisable(true);
                                    setStyle(tableView.getStyle());
                                }
                            } else {
                                setStyle(tableView.getStyle());
                            }

                            // Check if melt can be swapped
                             /*EvapScalesGatewayDao evapScalesGatewayDao = new EvapScalesGatewayDao();
                             if(currentRow.getItem().getInMeltId() != null && !currentRow.getItem().getInMeltId().equalsIgnoreCase("")) {
                             evapScalesGatewayDao.ifMeltIsInAnotherToolOrPocket(currentRow.getItem().getInMeltId(), currentEvapId.getValue().toString(), currentRow.getItem().getPocket());
                             }*/
                        }
                    };
                });
                
                tableColumnNoMelt.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, Boolean>("noMelt")
                );
                tableColumnNoMelt.setCellFactory(column -> {
                    return new CheckBoxTableCell<MetalUsageObject, Boolean>() {
                        @Override
                        public void updateItem(Boolean item, boolean empty) {
                            super.updateItem(item, empty);
                            
                            TableRow<MetalUsageObject> currentRow = getTableRow();
                            if (currentRow.getItem() != null && !empty) {
                                if (currentRow.getItem().getMeltState().equalsIgnoreCase("PREPPED") || currentRow.getItem().getIsPreppedWithNoMelt()) {
                                    setDisable(true);
                                    setEditable(false);
                                    this.setStyle("-fx-background-color: green");
                                } else if (currentRow.getItem().getPocket().equalsIgnoreCase("0")
                                        || currentRow.getItem().getMetal().equalsIgnoreCase("AU3")) {
                                    setDisable(true);
                                    setEditable(false);
                                    this.setStyle("-fx-background-color: lightgrey");
                                }
                                if (currentRow.getItem().getMakeMelt() == true || currentRow.getItem().getSwapMelt() == true) {
                                    currentRow.getItem().setNoMelt(false);
                                    this.setDisable(true);
                                    setStyle(tableView.getStyle());
                                }
                            } else {
                                setStyle(tableView.getStyle());
                            }
                            if (currentRow.getItem() != null && currentRow.getItem().getMetal().equalsIgnoreCase("AU3")) {
                                if (data.get(0) != null && data.get(0).getNoMelt() == true) {
                                    setDisable(false);
                                    setEditable(true);
                                    setStyle(tableView.getStyle());
                                }
                            }
                            /* if (currentRow.getItem() != null && currentRow.getItem().getMetal().equalsIgnoreCase("AU4")) {
                             if (data.get(1) != null && data.get(1).getNoMelt() == false) {
                             currentRow.getItem().setNoMelt(true);
                             //setDisable(false);
                             //setEditable(true);
                             //setStyle(tableView.getStyle());
                             }
                             } */
                        }
                    };
                });
                
                tableColumnOutMeltId.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("outMeltId")
                );
                // Use a TextFieldTableCell, so it can be edited
                //tableColumnOutMeltId.setCellFactory(TextFieldTableCell.<MetalUsageObject>forTableColumn()); 

                tableColumnOutMeltId.setCellFactory(
                        new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {
                            
                            @Override
                            public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                                return new TextFieldTableCell<MetalUsageObject, String>(new DefaultStringConverter()) {
                                    @Override
                                    public void updateItem(String s, boolean empty) {
                                        super.updateItem(s, empty);
                                        TableRow<MetalUsageObject> currentRow = getTableRow();
                                        if (currentRow.getItem() != null && !empty) {
                                            if ((currentRow.getItem().getMakeMelt() || currentRow.getItem().getSwapMelt())
                                            && currentRow.getItem().getIsMeltIdentified() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: red");
                                            } else if (currentRow.getItem().getNoMelt() && currentRow.getItem().getIsMeltIdentified() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                setText(null);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (currentRow.getItem().getMakeMelt() == false && currentRow.getItem().getSwapMelt() == false
                                            && currentRow.getItem().getNoMelt() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                setText(null);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (currentRow.getItem().getSwapMelt() == true && (currentRow.getItem().getMetal().equalsIgnoreCase("NI")
                                            || currentRow.getItem().getMetal().equalsIgnoreCase("PT") || currentRow.getItem().getMetal().equalsIgnoreCase("TI"))) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else {
                                                setDisable(false);
                                                setEditable(true);
                                                //setText(null);
                                                //if(s != null && !s.equalsIgnoreCase(""))
                                                //  item.setInMeltId(s);
                                                setStyle("");
                                            }
                                        }
                                    }
                                };
                            }
                        });

                // Set editing related event handlers (OnEditCommit)
                tableColumnOutMeltId.setOnEditCommit(new EventHandler<CellEditEvent<MetalUsageObject, String>>() {
                    @Override
                    public void handle(CellEditEvent<MetalUsageObject, String> t) {
                        MetalUsageObject metalUsageObj = ((MetalUsageObject) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()));
                        
                        if (t.getNewValue() != null && !t.getNewValue().equalsIgnoreCase("")) {
                            if (!t.getNewValue().equalsIgnoreCase(backUpInputMeltId)) {
                                OutMeltIdValidationDialogController.showDialog(t.getNewValue());
                            } else {
                                metalUsageObj.setOutMeltId(t.getNewValue());
                                //Store it in cache
                                List<MetalUsageObject> metalUsageObjectList = evapToDataMap.get(currentEvapId);
                                if (metalUsageObjectList != null) {
                                    for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                                        if (metalUsageObject.getMetal().trim().equalsIgnoreCase(metalUsageObj.getMetal().trim())) {
                                            metalUsageObject.setOutMeltId(t.getNewValue().trim());
                                        }
                                    }
                                    evapToDataMap.put(currentEvapId, metalUsageObjectList);
                                } else {
                                    evapToDataMap.put(currentEvapId, Arrays.asList(new MetalUsageObject(null, "AU4"), new MetalUsageObject(null, "AU3"), new MetalUsageObject(null, "AUGE"),
                                            new MetalUsageObject(null, "NI"), new MetalUsageObject(null, "PT"), new MetalUsageObject(null, "TI")));
                                }
                            }
                        }
                    }
                });
                
                tableColumnMeltWeight.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("meltWeight")
                );
                
                tableColumnMeltWeight.setCellFactory(
                        new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {
                            
                            @Override
                            public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                                return new TextFieldTableCell<MetalUsageObject, String>(new DefaultStringConverter()) {
                                    @Override
                                    public void updateItem(String s, boolean empty) {
                                        super.updateItem(s, empty);
                                        TableRow<MetalUsageObject> currentRow = getTableRow();
                                        if (currentRow.getItem() != null && !empty) {
                                            if ((currentRow.getItem().getMakeMelt() || currentRow.getItem().getSwapMelt()) && (currentRow.getItem().getMetal().equalsIgnoreCase("NI")
                                            || currentRow.getItem().getMetal().equalsIgnoreCase("PT") || currentRow.getItem().getMetal().equalsIgnoreCase("TI"))) {
                                                setDisable(false);
                                                setEditable(true);
                                                setStyle("");
                                            } else if ((currentRow.getItem().getMakeMelt() || currentRow.getItem().getSwapMelt()) && currentRow.getItem().getIsMeltIdentified() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: red");
                                            } else if (currentRow.getItem().getMakeMelt() == false && currentRow.getItem().getSwapMelt() == false
                                            && currentRow.getItem().getNoMelt() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                setText(null);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (currentRow.getItem().getNoMelt() == true && currentRow.getItem().getIsMeltIdentified() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else {
                                                setDisable(false);
                                                setEditable(true);
                                                //if(s != null && !s.equalsIgnoreCase(""))
                                                //  item.setInMeltId(s);
                                                setStyle("");
                                            }
                                        }
                                    }
                                };
                            }
                            
                        });

                // Set editing related event handlers (OnEditCommit)
                tableColumnMeltWeight.setOnEditCommit(new EventHandler<CellEditEvent<MetalUsageObject, String>>() {
                    @Override
                    public void handle(CellEditEvent<MetalUsageObject, String> t) {
                        MetalUsageObject metalUsageObj = ((MetalUsageObject) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()));
                        String meltScaleWeight = readScalesMesaurement(true, false, false);
                        if (meltScaleWeight.equalsIgnoreCase("")) {
                            meltScaleWeight = "0.0";
                        }
                        metalUsageObj.setMeltWeight(meltScaleWeight);
                        //Store it in cache
                        if (evapToDataMap.containsKey(currentEvapId)) {
                            //Store it in cache
                            List<MetalUsageObject> metalUsageObjectList = evapToDataMap.get(currentEvapId);
                            if (metalUsageObjectList != null) {
                                for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                                    if (metalUsageObject.getMetal().trim().equalsIgnoreCase(metalUsageObj.getMetal().trim())) {
                                        metalUsageObject.setMeltWeight(meltScaleWeight.trim());
                                        break;
                                    }
                                }
                                evapToDataMap.put(currentEvapId, metalUsageObjectList);
                            }
                        } else {
                            evapToDataMap.put(currentEvapId, Arrays.asList(new MetalUsageObject(null, "AU4"), new MetalUsageObject(null, "AU3"), new MetalUsageObject(null, "AUGE"),
                                    new MetalUsageObject(null, "NI"), new MetalUsageObject(null, "PT"), new MetalUsageObject(null, "TI")));
                        }
                    }
                });
                
                tableColumnInMeltId.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("inMeltId")
                );

                // Use a TextFieldTableCell, so it can be edited
                //tableColumnInMeltId.setCellFactory(TextFieldTableCell.<MetalUsageObject>forTableColumn()); 
                // https://stackoverflow.com/questions/10925882/disable-tablerow-based-on-data
                tableColumnInMeltId.setCellFactory(
                        new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {
                            
                            @Override
                            public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                                return new TextFieldTableCell<MetalUsageObject, String>(new DefaultStringConverter()) {
                                    @Override
                                    public void updateItem(String s, boolean b) {
                                        super.updateItem(s, b);
                                        if (!isEmpty()) {
                                            MetalUsageObject item = getTableView().getItems().get(getIndex());
                                            if (item.getMeltState().equalsIgnoreCase("PREPPED") || item.getIsPreppedWithNoMelt()) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: green");
                                            } else if (item.getPocket().equalsIgnoreCase("0")) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getMakeMelt() == false && item.getSwapMelt() == false
                                            && item.getNoMelt() == false && (item.getMetal().equalsIgnoreCase("AU3")
                                            || item.getMetal().equalsIgnoreCase("AU4") || item.getMetal().equalsIgnoreCase("AUGE"))) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if ((item.getSwapMelt() == true || item.getMakeMelt() == true) && item.getIsMeltIdentified() == false && (item.getMetal().equalsIgnoreCase("AU3")
                                            || item.getMetal().equalsIgnoreCase("AU4") || item.getMetal().equalsIgnoreCase("AUGE"))) {
                                                setDisable(false);
                                                setEditable(true);
                                                setStyle("");
                                            } else if ((item != null && (item.getIsMeltIdentified() == false || item.getIsQualifiedState() == false) && (item.getMetal().equalsIgnoreCase("AU3")
                                            || item.getMetal().equalsIgnoreCase("AU4") || item.getMetal().equalsIgnoreCase("AUGE")))) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getNoMelt() == true) {
                                                setDisable(true);
                                                setEditable(false);
                                                setText("");
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getMetal().equalsIgnoreCase("NI")
                                            || item.getMetal().equalsIgnoreCase("PT") || item.getMetal().equalsIgnoreCase("TI")) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else {
                                                setDisable(false);
                                                setEditable(true);
                                                //if(s != null && !s.equalsIgnoreCase(""))
                                                //  item.setInMeltId(s);
                                                setStyle("");
                                            }
                                        }
                                    }
                                };
                            }
                            
                        });

                // Set editing related event handlers (OnEditCommit)
                tableColumnInMeltId.setOnEditCommit(new EventHandler<CellEditEvent<MetalUsageObject, String>>() {
                    @Override
                    public void handle(CellEditEvent<MetalUsageObject, String> t) {
                        MetalUsageObject metalUsageObj = ((MetalUsageObject) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()));
                        //Do a database check
                        if (t.getNewValue() != null && !t.getNewValue().equalsIgnoreCase("")) {
                            if (metalUsageObj.getSwapMelt()) {
                                if (evapScalesGatewayDao.ifMeltIsInAnotherToolOrPocket(t.getNewValue(), currentEvapId.trim(), metalUsageObj.getPocket(), usernameLabel.getText().trim().toUpperCase())) {
                                    MeltIdValidationDialogController.showDialog(t.getNewValue());
                                    metalUsageObj.setInMeltId((!backUpInputMeltId.equalsIgnoreCase("") || backUpInputMeltId.equalsIgnoreCase("NA")) ? backUpInputMeltId : "NA");
                                    setBackUpInputMeltId(backUpInputMeltId);
                                    tableView.setItems(null);
                                    tableView.layout();
                                    tableView.setItems(FXCollections.observableList(data));
                                    tableView.refresh();
                                } else {
                                    metalUsageObj.setInMeltId(t.getNewValue());
                                    //Store it in cache
                                    List<MetalUsageObject> metalUsageObjectList = evapToDataMap.get(currentEvapId);
                                    if (metalUsageObjectList != null) {
                                        for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                                            if (metalUsageObject.getMetal().trim().equalsIgnoreCase(metalUsageObj.getMetal().trim())) {
                                                metalUsageObject.setInMeltId(t.getNewValue());
                                            }
                                        }
                                        evapToDataMap.put(currentEvapId, metalUsageObjectList);
                                    } else {
                                        evapToDataMap.put(currentEvapId, Arrays.asList(new MetalUsageObject(null, "AU4"), new MetalUsageObject(null, "AU3"), new MetalUsageObject(null, "AUGE"),
                                                new MetalUsageObject(null, "NI"), new MetalUsageObject(null, "PT"), new MetalUsageObject(null, "TI")));
                                    }
                                }
                            }
                            if (metalUsageObj.getMakeMelt()) {
                                if (evapScalesGatewayDao.ifMeltAlreadyInSystem(t.getNewValue())) {
                                    InMeltIdValidationDialogController.showDialog(t.getNewValue());
                                    metalUsageObj.setInMeltId((!backUpInputMeltId.equalsIgnoreCase("") || backUpInputMeltId.equalsIgnoreCase("NA")) ? backUpInputMeltId : "NA");
                                    setBackUpInputMeltId(backUpInputMeltId);
                                    tableView.setItems(null);
                                    tableView.layout();
                                    tableView.setItems(FXCollections.observableList(data));
                                    tableView.refresh();
                                } else {
                                    metalUsageObj.setInMeltId(t.getNewValue());
                                    //Store it in cache
                                    List<MetalUsageObject> metalUsageObjectList = evapToDataMap.get(currentEvapId);
                                    if (metalUsageObjectList != null) {
                                        for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                                            if (metalUsageObject.getMetal().trim().equalsIgnoreCase(metalUsageObj.getMetal().trim())) {
                                                metalUsageObject.setInMeltId(t.getNewValue());
                                            }
                                        }
                                        evapToDataMap.put(currentEvapId, metalUsageObjectList);
                                    } else {
                                        evapToDataMap.put(currentEvapId, Arrays.asList(new MetalUsageObject(null, "AU4"), new MetalUsageObject(null, "AU3"), new MetalUsageObject(null, "AUGE"),
                                                new MetalUsageObject(null, "NI"), new MetalUsageObject(null, "PT"), new MetalUsageObject(null, "TI")));
                                    }
                                }
                            }
                        }
                    }
                });
                
                tableColumnInLocation.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("inLocation")
                );
                
                tableColumnPreAdd.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("preAdd")
                );
                // Use a TextFieldTableCell, so it can be edited
                tableColumnPreAdd.setCellFactory(
                        new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {
                            
                            @Override
                            public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                                return new TextFieldTableCell<MetalUsageObject, String>(new DefaultStringConverter()) {
                                    @Override
                                    public void updateItem(String s, boolean b) {
                                        super.updateItem(s, b);
                                        if (!isEmpty()) {
                                            MetalUsageObject item = getTableView().getItems().get(getIndex());
                                            if (item.getMeltState().equalsIgnoreCase("PREPPED") || item.getIsPreppedWithNoMelt()) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: green");
                                            } else if (item.getPocket().equalsIgnoreCase("0")) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getSwapMelt() || item.getMakeMelt()) {
                                                setDisable(false);
                                                setEditable(true);
                                                setStyle("");
                                            } else if ((item != null && (item.getIsMeltIdentified() == false || item.getIsQualifiedState() == false) && (item.getMetal().equalsIgnoreCase("AU3")
                                            || item.getMetal().equalsIgnoreCase("AU4") || item.getMetal().equalsIgnoreCase("AUGE")))) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getNoMelt() == true) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else {
                                                setDisable(false);
                                                setEditable(true);
                                                //if(s != null && !s.equalsIgnoreCase(""))
                                                //  item.setInMeltId(s);
                                                setStyle("");
                                            }
                                        }
                                    }
                                };
                            }
                            
                        });
                // Set editing related event handlers (OnEditCommit)
                tableColumnPreAdd.setOnEditCommit(new EventHandler<CellEditEvent<MetalUsageObject, String>>() {
                    @Override
                    public void handle(CellEditEvent<MetalUsageObject, String> t) {
                        MetalUsageObject metalUsageObj = ((MetalUsageObject) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()));
                        String preAddScalesValue = readScalesMesaurement(true, false, false);
                        if (preAddScalesValue.equalsIgnoreCase("")) {
                            preAddScalesValue = "0.0";
                        }
                        metalUsageObj.setPreAdd(preAddScalesValue);
                        //Store it in cache
                        List<MetalUsageObject> metalUsageObjectList = evapToDataMap.get(currentEvapId);
                        if (metalUsageObjectList != null) {
                            for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                                if (metalUsageObject.getMetal().trim().equalsIgnoreCase(metalUsageObj.getMetal().trim())) {
                                    metalUsageObject.setPreAdd(preAddScalesValue.trim());
                                }
                            }
                            evapToDataMap.put(currentEvapId, metalUsageObjectList);
                        } else {
                            evapToDataMap.put(currentEvapId, Arrays.asList(new MetalUsageObject(null, "AU4"), new MetalUsageObject(null, "AU3"), new MetalUsageObject(null, "AUGE"),
                                    new MetalUsageObject(null, "NI"), new MetalUsageObject(null, "PT"), new MetalUsageObject(null, "TI")));
                        }
                    }
                });
                
                tableColumnQtyLgPellet.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("qtyLgPellet")
                );
                // Use a TextFieldTableCell, so it can be edited
                tableColumnQtyLgPellet.setCellFactory(
                        new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {
                            
                            @Override
                            public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                                return new TextFieldTableCell<MetalUsageObject, String>(new DefaultStringConverter()) {
                                    @Override
                                    public void updateItem(String s, boolean b) {
                                        super.updateItem(s, b);
                                        if (!isEmpty()) {
                                            MetalUsageObject item = getTableView().getItems().get(getIndex());
                                            if (item.getMeltState().equalsIgnoreCase("PREPPED") || item.getIsPreppedWithNoMelt()) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: green");
                                            } else if (item.getPocket().equalsIgnoreCase("0")) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getSwapMelt() || item.getMakeMelt()) {
                                                setDisable(false);
                                                setEditable(true);
                                                setStyle("");
                                            } else if (item != null && (item.getIsMeltIdentified() == false || item.getIsQualifiedState() == false) && (item.getMetal().equalsIgnoreCase("AU3")
                                            || item.getMetal().equalsIgnoreCase("AU4") || item.getMetal().equalsIgnoreCase("AUGE"))) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getNoMelt() == true) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else {
                                                setDisable(false);
                                                setEditable(true);
                                                //if(s != null && !s.equalsIgnoreCase(""))
                                                //  item.setInMeltId(s);
                                                setStyle("");
                                            }
                                        }
                                    }
                                };
                            }
                            
                        });
                // Set editing related event handlers (OnEditCommit)
                tableColumnQtyLgPellet.setOnEditCommit(new EventHandler<CellEditEvent<MetalUsageObject, String>>() {
                    @Override
                    public void handle(CellEditEvent<MetalUsageObject, String> t) {
                        MetalUsageObject metalUsageObj = ((MetalUsageObject) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()));
                        String scaleLgPelletValue = readScalesMesaurement(false, true, false);
                        if (scaleLgPelletValue.equalsIgnoreCase("")) {
                            scaleLgPelletValue = "0.0";
                        } else {
                            scaleLgPelletValue = Double.toString(Double.parseDouble(scaleLgPelletValue) - Double.parseDouble(metalUsageObj.getPreAdd()));
                        }
                        metalUsageObj.setQtyLgPellet(scaleLgPelletValue);
                        //Store it in cache
                        List<MetalUsageObject> metalUsageObjectList = evapToDataMap.get(currentEvapId);
                        if (metalUsageObjectList != null) {
                            for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                                if (metalUsageObject.getMetal().trim().equalsIgnoreCase(metalUsageObj.getMetal().trim())) {
                                    metalUsageObject.setQtyLgPellet(scaleLgPelletValue);
                                }
                            }
                            evapToDataMap.put(currentEvapId, metalUsageObjectList);
                        } else {
                            evapToDataMap.put(currentEvapId, Arrays.asList(new MetalUsageObject(null, "AU4"), new MetalUsageObject(null, "AU3"), new MetalUsageObject(null, "AUGE"),
                                    new MetalUsageObject(null, "NI"), new MetalUsageObject(null, "PT"), new MetalUsageObject(null, "TI")));
                        }
                    }
                });
                
                tableColumnQtySmPellet.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("qtySmPellet")
                );
                // Use a TextFieldTableCell, so it can be edited
                tableColumnQtySmPellet.setCellFactory(
                        new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {
                            
                            @Override
                            public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                                return new TextFieldTableCell<MetalUsageObject, String>(new DefaultStringConverter()) {
                                    @Override
                                    public void updateItem(String s, boolean b) {
                                        super.updateItem(s, b);
                                        if (!isEmpty()) {
                                            MetalUsageObject item = getTableView().getItems().get(getIndex());
                                            if (item.getMeltState().equalsIgnoreCase("PREPPED") || item.getIsPreppedWithNoMelt()) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: green");
                                            } else if (item.getPocket().equalsIgnoreCase("0")) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getMetal().equalsIgnoreCase("NI")
                                            || item.getMetal().equalsIgnoreCase("PT") || item.getMetal().equalsIgnoreCase("TI")) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if ((item.getSwapMelt() || item.getMakeMelt()) && (item.getMetal().equalsIgnoreCase("AU3")
                                            || item.getMetal().equalsIgnoreCase("AU4"))) {
                                                setDisable(false);
                                                setEditable(true);
                                                setStyle("");
                                            } else if (item.getMakeMelt() && item.getMetal().equalsIgnoreCase("AUGE")) {
                                                setDisable(false);
                                                setEditable(true);
                                                setStyle("");
                                            } else if (item.getMetal().equalsIgnoreCase("AUGE")) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item != null && (item.getIsMeltIdentified() == false || item.getIsQualifiedState() == false)) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getNoMelt() == true) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else {
                                                setDisable(false);
                                                setEditable(true);
                                                //if(s != null && !s.equalsIgnoreCase(""))
                                                //  item.setInMeltId(s);
                                                setStyle("");
                                            }
                                        }
                                    }
                                };
                            }
                            
                        });
                // Set editing related event handlers (OnEditCommit)
                tableColumnQtySmPellet.setOnEditCommit(new EventHandler<CellEditEvent<MetalUsageObject, String>>() {
                    @Override
                    public void handle(CellEditEvent<MetalUsageObject, String> t) {
                        MetalUsageObject metalUsageObj = ((MetalUsageObject) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()));
                        String scalesMeasurementSmallPlt = readScalesMesaurement(true, false, false);
                        if (scalesMeasurementSmallPlt.equalsIgnoreCase("")) {
                            scalesMeasurementSmallPlt = "0.0";
                        } else {
                            scalesMeasurementSmallPlt = Double.toString(Double.parseDouble(scalesMeasurementSmallPlt) - Double.parseDouble(metalUsageObj.getPreAdd()) 
                                    - Double.parseDouble(metalUsageObj.getQtyLgPellet()));
                        }
                        if (Double.parseDouble(scalesMeasurementSmallPlt) > 26.7 && !metalUsageObj.getMetal().equalsIgnoreCase("AUGE")) {
                            SmallPelletDialogController.showDialog();
                        }
                        metalUsageObj.setQtySmPellet(scalesMeasurementSmallPlt);

                        //Store it in cache
                        List<MetalUsageObject> metalUsageObjectList = evapToDataMap.get(currentEvapId);
                        if (metalUsageObjectList != null) {
                            for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                                if (metalUsageObject.getMetal().trim().equalsIgnoreCase(metalUsageObj.getMetal().trim())) {
                                    metalUsageObject.setQtySmPellet(scalesMeasurementSmallPlt);
                                }
                            }
                            evapToDataMap.put(currentEvapId, metalUsageObjectList);
                        } else {
                            evapToDataMap.put(currentEvapId, Arrays.asList(new MetalUsageObject(null, "AU4"), new MetalUsageObject(null, "AU3"), new MetalUsageObject(null, "AUGE"),
                                    new MetalUsageObject(null, "NI"), new MetalUsageObject(null, "PT"), new MetalUsageObject(null, "TI")));
                        }
                    }
                });
                
                tableColumnMetal.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("metal")
                );
                tableColumnPocket.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("pocket")
                );
                tableColumnMetalLgPlt.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("metalLargePlt")
                );
                tableColumnMetalSmPlt.setCellValueFactory(
                        new PropertyValueFactory<MetalUsageObject, String>("metalSmallPlt")
                );

                // Reference: https://gist.github.com/haisi/0a82e17daf586c9bab52
                tableColumnOutLocation.setCellValueFactory(new PropertyValueFactory<MetalUsageObject, String>("name"));
                /*Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>> outLocCellFactory = 
                 new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {

                 @Override
                 public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                 MetalUsageObject item = getTableView().getItems().get(getIndex());
                 ComboBoxTableCell<MetalUsageObject, String> golog = new ComboBoxTableCell<MetalUsageObject, String>(
                 new DefaultStringConverter(), outLocationValues);

                 return golog;
                 }
                 };
                 tableColumnOutLocation.setCellFactory(outLocCellFactory); */
                //tableColumnOutLocation.setCellFactory(ComboBoxTableCell.forTableColumn(outLocationValues)); 
                //tableColumnOutLocation.setStyle("-fx-background-color: lightgrey");

                tableColumnOutLocation.setCellFactory(
                        new Callback<TableColumn<MetalUsageObject, String>, TableCell<MetalUsageObject, String>>() {
                            
                            @Override
                            public TableCell<MetalUsageObject, String> call(TableColumn<MetalUsageObject, String> paramTableColumn) {
                                return new ComboBoxTableCell<MetalUsageObject, String>(new DefaultStringConverter(), outLocationValues) {
                                    @Override
                                    public void updateItem(String s, boolean b) {
                                        super.updateItem(s, b);
                                        if (!isEmpty()) {
                                            MetalUsageObject item = getTableView().getItems().get(getIndex());
                                            if (item.getMeltState().equalsIgnoreCase("PREPPED") || item.getIsPreppedWithNoMelt()) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: green");
                                            } else if (item.getMakeMelt() == false && item.getSwapMelt() == false
                                            && item.getNoMelt() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if ((item.getMakeMelt() || item.getSwapMelt()) && item.getIsMeltIdentified() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: red");
                                            } else if (item.getNoMelt() && item.getIsMeltIdentified() == false) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else if (item.getSwapMelt() == true && (item.getMetal().equalsIgnoreCase("NI")
                                            || item.getMetal().equalsIgnoreCase("PT") || item.getMetal().equalsIgnoreCase("TI"))) {
                                                setDisable(true);
                                                setEditable(false);
                                                this.setStyle("-fx-background-color: lightgrey");
                                            } else {
                                                setDisable(false);
                                                setEditable(true);
                                                setStyle("");
                                            }
                                        }
                                    }
                                };
                            }
                            
                        });
                
                tableColumnOutLocation.setOnEditCommit(new EventHandler<CellEditEvent<MetalUsageObject, String>>() {
                    @Override
                    public void handle(CellEditEvent<MetalUsageObject, String> t) {
                        if (t.getNewValue().trim().contains("SAFE") && !evapScalesGatewayDao.isSafeLocationUpdationAllowed(usernameLabel.getText().trim().toUpperCase())) {
                            UserAccessLevelDialogController.showDialog(usernameLabel.getText().trim().toUpperCase());
                        } else {
                            ((MetalUsageObject) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())).setOutLocation(FXCollections.observableArrayList(new ArrayList<>(Arrays.asList(t.getNewValue()))));
                        }
                    }
                });

                // https://gist.github.com/jewelsea/3081826
                cancelPrep = new TableColumn<>("CancelPrep");
                cancelPrep.setMaxWidth(6000.0);
                //cancelPrep.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

                cancelPrep.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MetalUsageObject, Boolean>, ObservableValue<Boolean>>() {
                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<MetalUsageObject, Boolean> features) {
                        return new SimpleBooleanProperty(features.getValue().getMeltState().trim().equalsIgnoreCase("PREPPED"));
                    }
                });

                // create a cell value factory with an add button for each row in the table.
                cancelPrep.setCellFactory(new Callback<TableColumn<MetalUsageObject, Boolean>, TableCell<MetalUsageObject, Boolean>>() {
                    @Override
                    public TableCell<MetalUsageObject, Boolean> call(TableColumn<MetalUsageObject, Boolean> personBooleanTableColumn) {
                        return new CancelPrepCell(tableView);
                    }
                });
                
                
                prepTime = new TableColumn<>("PrepTime");
                prepTime.setMaxWidth(13500.0);

                prepTime.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MetalUsageObject, Boolean>, ObservableValue<Boolean>>() {
                    @Override
                    public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<MetalUsageObject, Boolean> features) {
                        return new SimpleBooleanProperty(features.getValue().getMeltState().trim().equalsIgnoreCase("PREPPED"));
                    }
                });

                // create a cell value factory with a prep time for each row in the table.
                prepTime.setCellFactory(new Callback<TableColumn<MetalUsageObject, Boolean>, TableCell<MetalUsageObject, Boolean>>() {
                    @Override
                    public TableCell<MetalUsageObject, Boolean> call(TableColumn<MetalUsageObject, Boolean> personBooleanTableColumn) {
                        return new PrepTimeCell(tableView);
                    }
                });

                /*
                 cancelPrep.setCellFactory(param -> new TableCell<MetalUsageObject, Boolean>() {
                 private final Button prepCellButton = new Button("Cancel");

                 @Override
                 protected void updateItem(MetalUsageObject metalUsageObject, boolean empty) {
                 super.updateItem(metalUsageObject, empty);

                 if (metalUsageObject == null) {
                 setGraphic(null);
                 return;
                 }

                 setGraphic(prepCellButton);
                 prepCellButton.setOnAction(event -> data.remove(metalUsageObject));
                 }
                 });*/

                /*
                 Callback<TableColumn<MetalUsageObject, MetalUsageObject>, TableCell<MetalUsageObject, MetalUsageObject>> cellFactory
                 = new Callback<TableColumn<MetalUsageObject, MetalUsageObject>, TableCell<MetalUsageObject, MetalUsageObject>>() {
                 @Override
                 public TableCell call(final TableColumn<MetalUsageObject, MetalUsageObject> param) {
                 final TableCell<MetalUsageObject, MetalUsageObject> cell = new TableCell<MetalUsageObject, MetalUsageObject>() {
                 final Button prepCellButton = new Button("Cancel Prep");

                 @Override
                 public void updateItem(String item, boolean empty) {
                 super.updateItem(item, empty);
                 if (empty) {
                 //setGraphic(null);
                 setText(null);
                 } else {
                 setGraphic(prepCellButton);
                 prepCellButton.setOnAction(
                 event -> System.out.println(item));
                 }
                 }
                 };
                 return cell;
                 }
                 }; 
                 cancelPrep.setCellFactory(cellFactory); */
                tableView.getColumns().addAll(cancelPrep);
                tableView.getColumns().addAll(prepTime);
                
                
                tableView.setItems(data);
            }
        });
        
    }
    
    private class PrepTimeCell extends TableCell<MetalUsageObject, Boolean> {

        /**
         * PrepTimeCell constructor
         */
        PrepTimeCell(final TableView table) {
            setText("");
            getStyleClass().add("pt-cell");
        }

        /**
         * places prep time in the row only if the row is prepped
         */
        @Override
        protected void updateItem(Boolean item, boolean empty) {
            MetalUsageObject metalUsageObject = null;
            if (getTableView().getItems() != null && getIndex() >= 0 && getIndex() <= 5) {
                metalUsageObject = getTableView().getItems().get(getIndex());
            }
            boolean ifPrepped = false;
            boolean ifPreppedWithNoMelt = false;
            if (metalUsageObject != null) {
                ifPrepped = metalUsageObject.getMeltState().trim().equalsIgnoreCase("PREPPED");
                ifPreppedWithNoMelt = metalUsageObject.getIsPreppedWithNoMelt();
            }
            super.updateItem(item, empty);
            if (!empty && (ifPrepped || ifPreppedWithNoMelt)) {
                if (evapScalesGatewayDao == null) {
                    evapScalesGatewayDao = new EvapScalesGatewayDao();
                }
                String prepTime = evapScalesGatewayDao.fetchPrepTime(currentEvapId, metalUsageObject.getMetal());
                setText(prepTime);
            } else {
                setGraphic(null);
                setStyle("-fx-background-color: lightgrey");
            }
        }
    }
    
    private class CancelPrepCell extends TableCell<MetalUsageObject, Boolean> {
        
        final Button cancelPrepButton = new Button("Cancel");

        /**
         * CancelPrepCell constructor
         */
        CancelPrepCell(final TableView table) {
            cancelPrepButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    MetalUsageObject metalUsageObject = getTableView().getItems().get(getIndex());
                    if (evapScalesGatewayDao == null) {
                        evapScalesGatewayDao = new EvapScalesGatewayDao();
                    }
                    try {
                        String prepId = evapScalesGatewayDao.fetchPrepId(currentEvapId, metalUsageObject.getMetal());
                        if (prepId != null) {
                            evapScalesGatewayDao.evapInvalidatePrep(currentEvapId, "ABORT", prepId, metalUsageObject.getMetal());
                            evapScalesGatewayDao.updateToolState(currentEvapId, "PREP_CANCEL", "UNKNOWN");
                        }
                    } catch (SQLException sqle) {
                        java.util.logging.Logger.getLogger(MainViewController.class.getName()).log(java.util.logging.Level.SEVERE, null, sqle);
                    } catch (NamingException ne) {
                        java.util.logging.Logger.getLogger(MainViewController.class.getName()).log(java.util.logging.Level.SEVERE, null, ne);
                    }
                    setGraphic(cancelPrepButton);
                    setText(null);
                    cancelPrepButton.setStyle(String.format("-fx-font-size: %dpx;", (int) (0.45 * 35)));
                    populateTable(currentEvapId);
                }
            });
        }

        /**
         * places cancel prep button in the row only if the row is not prepped
         */
        @Override
        protected void updateItem(Boolean item, boolean empty) {
            MetalUsageObject metalUsageObject = null;
            if (getTableView().getItems() != null && getIndex() >= 0 && getIndex() <= 5) {
                metalUsageObject = getTableView().getItems().get(getIndex()); //throwing NPE for Evap13
            }
            boolean ifPrepped = false;
            boolean ifPreppedWithNoMelt = false;
            if (metalUsageObject != null) {
                ifPrepped = metalUsageObject.getMeltState().trim().equalsIgnoreCase("PREPPED");
                ifPreppedWithNoMelt = metalUsageObject.getIsPreppedWithNoMelt();
            }
            super.updateItem(item, empty);
            if (!empty && (ifPrepped || ifPreppedWithNoMelt)) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(cancelPrepButton);
                cancelPrepButton.setStyle(String.format("-fx-font-size: %dpx;", (int) (0.45 * 35)));
            } else {
                setGraphic(null);
            }
        }
    }
    
    @FXML
    public void verifyEvapPrep(ActionEvent event) throws SQLException, NamingException {
        String evapPocket = "";
        String mainAddWeight = "";
        String subAddWeight = "";
        String content = "";
        
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            evapPocket = tableView.getSelectionModel().getSelectedItem().getMetal();
            mainAddWeight = tableView.getSelectionModel().getSelectedItem().getQtyLgPellet();
            subAddWeight = tableView.getSelectionModel().getSelectedItem().getQtySmPellet();
        }
        
        if (evapPocket.equalsIgnoreCase("AU4") || evapPocket.equalsIgnoreCase("AU3")) {
            content = String.format(evapPocket + " Large Pellet " + mainAddWeight + "\n" + evapPocket + " Small Pellet " + subAddWeight + "\n");
        } else {
            content = evapPocket + " " + mainAddWeight;
        }
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Verify Metal Added");
        alert.setHeaderText("Are your add quantities correct?");
        alert.setContentText(content);
        ButtonType okButton = new ButtonType("Yes");
        ButtonType noButton = new ButtonType("No");
        alert.getButtonTypes().setAll(okButton, noButton);
        alert.showAndWait().ifPresent(response -> {
            if (response == okButton) {
                try {
                    submitButtonAction(event);
                } catch (SQLException sqle) {
                    java.util.logging.Logger.getLogger(MainViewController.class.getName()).log(java.util.logging.Level.SEVERE, null, sqle);
                } catch (NamingException ne) {
                    java.util.logging.Logger.getLogger(MainViewController.class.getName()).log(java.util.logging.Level.SEVERE, null, ne);
                }
            }
        });
    }
    
    private void submitButtonAction(ActionEvent event) throws SQLException, NamingException {
        String meltIdToPass = "";
        String outMeltId = "";
        String meltLocationToPass = "";
        String outLocation = "";
        String meltWeight = "0.0";
        boolean isMeltQualified = false;
        String evapPocket = null;
        String baseWeight = "";
        String mainAddWeight = "";
        String subAddWeight = "";
        boolean makeMeltOptionSelected = false;
        boolean swapMeltOptionSelected = false;
        boolean noMeltOptionSelected = false;
        boolean isMeltIdentified = false;
        String metal = "";
        PrepPocketStatusMessage prepStatusCallOne = null;
        double postWeight = 0.0;
        
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            meltLocationToPass = tableView.getSelectionModel().getSelectedItem().getInLocation();
            meltIdToPass = tableView.getSelectionModel().getSelectedItem().getInMeltId();
            outMeltId = tableView.getSelectionModel().getSelectedItem().getOutMeltId();
            isMeltQualified = tableView.getSelectionModel().getSelectedItem().getIsQualifiedState();
            baseWeight = tableView.getSelectionModel().getSelectedItem().getPreAdd();
            mainAddWeight = tableView.getSelectionModel().getSelectedItem().getQtyLgPellet();
            subAddWeight = tableView.getSelectionModel().getSelectedItem().getQtySmPellet();
            evapPocket = tableView.getSelectionModel().getSelectedItem().getMetal();
            makeMeltOptionSelected = tableView.getSelectionModel().getSelectedItem().getMakeMelt();
            swapMeltOptionSelected = tableView.getSelectionModel().getSelectedItem().getSwapMelt();
            noMeltOptionSelected = tableView.getSelectionModel().getSelectedItem().getNoMelt();
            isMeltIdentified = tableView.getSelectionModel().getSelectedItem().getIsMeltIdentified();
            meltWeight = tableView.getSelectionModel().getSelectedItem().getMeltWeight();
            outLocation = tableView.getSelectionModel().getSelectedItem().getOutLocation().toString();
            metal = tableView.getSelectionModel().getSelectedItem().getMetal();
            postWeight = Double.parseDouble(mainAddWeight) + Double.parseDouble(subAddWeight);
        }
        
        if (!outLocation.equalsIgnoreCase("")) {
            outLocation = outLocation.replace("[", "");
            outLocation = outLocation.replace("]", "");
        }
        
        if (evapScalesGatewayDao == null) {
            evapScalesGatewayDao = new EvapScalesGatewayDao();
        }
        
        if (evapPocket != null && canPrep(evapPocket, baseWeight, mainAddWeight, subAddWeight, noMeltOptionSelected, isMeltIdentified, outMeltId, outLocation, meltWeight, meltIdToPass)) {
            //No Melt Option, No melt Identified
            if (noMeltOptionSelected && !isMeltIdentified) {
                String currentPrepId = evapScalesGatewayDao.createOrRetrievePrepId(currentEvapId.trim());
                PrepPocketStatusMessage prepStatusCall = evapScalesGatewayDao.updateEvapPocketWithNoMelt(currentEvapId.trim(), evapPocket, "PREPPED", currentPrepId != null ? currentPrepId : UUID.randomUUID().toString());
                if (prepStatusCall.getStatusCode() > 0) {
                    showAlertMessage(prepStatusCall);
                }
            } else if (noMeltOptionSelected && isMeltIdentified) {
                if (!outMeltId.trim().equalsIgnoreCase("") && !outLocation.equalsIgnoreCase("") && !meltWeight.equalsIgnoreCase("")) {
                    String currentPrepId = evapScalesGatewayDao.createOrRetrievePrepId(currentEvapId.trim());
                    PrepPocketStatusMessage prepStatusCallThree = evapScalesGatewayDao.updateEvapPocketWithNoMelt(currentEvapId.trim(), evapPocket, "PREPPED", currentPrepId != null ? currentPrepId : UUID.randomUUID().toString());
                    if (prepStatusCallThree.getStatusCode() > 0) {
                        showAlertMessage(prepStatusCallThree);
                    }
                    PrepPocketStatusMessage prepStatusCallTwo = evapScalesGatewayDao.evapRemoveMelt(outMeltId, currentEvapId.trim(), outLocation, Double.parseDouble(meltWeight));
                    if (prepStatusCallTwo.getStatusCode() > 0) {
                        showAlertMessage(prepStatusCallTwo);
                    }
                }
            } else if (swapMeltOptionSelected && !isMeltIdentified) {
                if (evapPocket.equalsIgnoreCase("AU3") || evapPocket.equalsIgnoreCase("AU4") || evapPocket.equalsIgnoreCase("AUGE")) {
                    if (!meltIdToPass.trim().equalsIgnoreCase("")) {
                        PrepPocketStatusMessage prepStatusCall = evapScalesGatewayDao.evapUpdateMelt(meltIdToPass, currentEvapId.trim(), evapScalesGatewayDao.fetchQualStatus(meltIdToPass.trim()), postWeight);
                        if (prepStatusCall.getStatusCode() > 0) {
                            showAlertMessage(prepStatusCall);
                        }
                    }
                }
            } else if (swapMeltOptionSelected && isMeltIdentified) {
                if (!meltIdToPass.trim().equalsIgnoreCase("") && !outMeltId.trim().equalsIgnoreCase("") && !outLocation.equalsIgnoreCase("") && !meltWeight.equalsIgnoreCase("")) {
                    PrepPocketStatusMessage prepStatusCallThree = evapScalesGatewayDao.evapRemoveMelt(outMeltId, currentEvapId.trim(), outLocation, Double.parseDouble(meltWeight));
                    if (prepStatusCallThree.getStatusCode() > 0) {
                        showAlertMessage(prepStatusCallThree);
                    }
                    PrepPocketStatusMessage prepStatusCallTwo = evapScalesGatewayDao.evapUpdateMelt(meltIdToPass, currentEvapId.trim(), evapScalesGatewayDao.fetchQualStatus(meltIdToPass.trim()), postWeight);
                    if (prepStatusCallTwo.getStatusCode() > 0) {
                        showAlertMessage(prepStatusCallTwo);
                    }
                }
            } else if (makeMeltOptionSelected && isMeltIdentified) {
                if (!meltIdToPass.trim().equalsIgnoreCase("") && !outMeltId.trim().equalsIgnoreCase("") && !outLocation.equalsIgnoreCase("") && !meltWeight.equalsIgnoreCase("")) {
                    prepStatusCallOne = evapScalesGatewayDao.evapRemoveMelt(outMeltId, currentEvapId.trim(), outLocation, Double.parseDouble(meltWeight));
                    if (prepStatusCallOne.getStatusCode() > 0) {
                        showAlertMessage(prepStatusCallOne);
                    }
                    if (metal.equalsIgnoreCase("AU3") || metal.equalsIgnoreCase("AU4")) {
                        metal = "AU";
                    }
                    PrepPocketStatusMessage prepStatusCallTwo = evapScalesGatewayDao.evapMakeNewMelt(meltIdToPass, currentEvapId.trim(), metal, postWeight);
                    if (prepStatusCallTwo.getStatusCode() > 0) {
                        showAlertMessage(prepStatusCallTwo);
                    }
                }
            } else if (makeMeltOptionSelected && !isMeltIdentified) {
                if (!meltIdToPass.trim().equalsIgnoreCase("")) {
                    if (metal.equalsIgnoreCase("AU3") || metal.equalsIgnoreCase("AU4")) {
                        metal = "AU";
                    }
                    PrepPocketStatusMessage prepStatusCall = evapScalesGatewayDao.evapMakeNewMelt(meltIdToPass, currentEvapId.trim(), metal, postWeight);
                    if (prepStatusCall.getStatusCode() > 0) {
                        showAlertMessage(prepStatusCall);
                    }
                }
            }
            
            if (!noMeltOptionSelected && !swapMeltOptionSelected && !makeMeltOptionSelected) {
                if (evapPocket.equalsIgnoreCase("AU3") || evapPocket.equalsIgnoreCase("AU4") || evapPocket.equalsIgnoreCase("AUGE")) {
                    if (!meltIdToPass.trim().equalsIgnoreCase("")) {
                        PrepPocketStatusMessage prepStatusCall = evapScalesGatewayDao.evapUpdateMelt(meltIdToPass, currentEvapId.trim(), evapScalesGatewayDao.fetchQualStatus(meltIdToPass.trim()), postWeight);
                        if (prepStatusCall.getStatusCode() > 0) {
                            showAlertMessage(prepStatusCall);
                        }
                    }
                }
            }
            
            if (!noMeltOptionSelected) {
                String currentPrepId = evapScalesGatewayDao.createOrRetrievePrepId(currentEvapId.trim());
                //For AUGE pass subAddWeight as 0
                PrepPocketStatusMessage prepStatusCall = evapScalesGatewayDao.evapPrepPocket(currentEvapId.trim(), currentEvapId.trim(), evapPocket.trim(), !meltIdToPass.trim().equalsIgnoreCase("") ? meltIdToPass.trim() : currentEvapId.trim() + "_" + evapPocket.trim(), Double.parseDouble(baseWeight),
                        Double.parseDouble(mainAddWeight), evapPocket.trim().equalsIgnoreCase("AUGE") ? 0.0 : !subAddWeight.equalsIgnoreCase("") ? Double.parseDouble(subAddWeight) : 0.0,
                        currentPrepId != null ? currentPrepId : UUID.randomUUID().toString(), usernameLabel.getText().trim().toUpperCase());
                if (prepStatusCall.getStatusCode() == 0 && ifFullyPrepped()) { //Check when all the pockets are prepped
                    evapScalesGatewayDao.updateToolState(currentEvapId.trim(), "PREPPED", "UNKNOWN");
                } else {
                    if (prepStatusCallOne != null) {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Error: Prep Status");
                        alert.setHeaderText("Prep Status:");
                        alert.setContentText("Melt was removed but prepping of new melt is incomplete\n" + prepStatusCall.getStatusMessage());
                        ButtonType okButton = new ButtonType("Ok");
                        alert.getButtonTypes().setAll(okButton);
                        alert.showAndWait();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Error: Prep Status");
                        alert.setHeaderText("Prep Status:");
                        alert.setContentText(prepStatusCall.getStatusMessage());
                        ButtonType okButton = new ButtonType("Ok");
                        alert.getButtonTypes().setAll(okButton);
                        alert.showAndWait();
                    }
                }
            }
        } else {
            if (meltIdToPass.trim().equalsIgnoreCase("NA")) {
                ErrorPrepPocketController.showDialog("Please enter a valid In Melt Id", "Invalid input Melt Id");
            } else if (evapPocket.equalsIgnoreCase("AU3") || evapPocket.equalsIgnoreCase("AU4") || evapPocket.equalsIgnoreCase("AUGE")) {
                ErrorPrepPocketController.showDialog("Please weigh-in pre, large pellet, small pellet and post", "Please try again");
            } else {
                ErrorPrepPocketController.showDialog("Please weigh-in pre, large pellet and post", "Please try again");
            }
        }

        // populate the refreshed data
        populateTable(currentEvapId);
    }
    
    private boolean ifFullyPrepped() {
        boolean notFullyPrepped = false;
        Iterator<MetalUsageObject> metalUsjObjIterator = data.iterator();
        while (metalUsjObjIterator.hasNext()) {
            MetalUsageObject metalUsageObject = metalUsjObjIterator.next();
            if (!metalUsageObject.getMeltState().equalsIgnoreCase("PREPPED") && !metalUsageObject.getPocket().equalsIgnoreCase("0")) {
                if (!metalUsageObject.getIsPreppedWithNoMelt()) {
                    notFullyPrepped = true;
                    break;
                } 
            }
        }
        return notFullyPrepped;
    }
    
    
    private void showAlertMessage(PrepPocketStatusMessage prepStatusCall) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Error: Prep Status");
        alert.setHeaderText("Prep Status:");
        alert.setContentText(prepStatusCall.getStatusMessage());
        ButtonType okButton = new ButtonType("Ok");
        alert.getButtonTypes().setAll(okButton);
        alert.showAndWait();
    }
    
    private boolean canPrep(String pocketNum, String baseWeight, String mainAddWeight, String subAddWeight, boolean noMeltSelectedOption, boolean isMeltIdentified,
            String outMeltId, String outLocation, String meltWeight, String inMeltId) {
        boolean ifCanPrep = false;
        if (inMeltId.trim().equalsIgnoreCase("NA")) {
            return ifCanPrep;
        }
        if (pocketNum.equalsIgnoreCase("AU3") || pocketNum.equalsIgnoreCase("AU4")) {
            if (!noMeltSelectedOption) {
                if (!baseWeight.equalsIgnoreCase("") && !mainAddWeight.equalsIgnoreCase("")
                        && !subAddWeight.equalsIgnoreCase("")) {
                    ifCanPrep = true;
                }
            } else {
                if (isMeltIdentified) {
                    if (!outMeltId.equalsIgnoreCase("") && !outLocation.equalsIgnoreCase("")
                            && !meltWeight.equalsIgnoreCase("")) {
                        ifCanPrep = true;
                    }
                } else {
                    ifCanPrep = true;
                }
            }
        } else if (pocketNum.equalsIgnoreCase("AUGE")) {
            if (!noMeltSelectedOption) {
                if (!baseWeight.equalsIgnoreCase("") && !mainAddWeight.equalsIgnoreCase("")) {
                    ifCanPrep = true;
                }
            } else {
                if (isMeltIdentified) {
                    if (!outMeltId.equalsIgnoreCase("") && !outLocation.equalsIgnoreCase("")
                            && !meltWeight.equalsIgnoreCase("")) {
                        ifCanPrep = true;
                    }
                } else {
                    ifCanPrep = true;
                }
            }
        } else if (pocketNum.equalsIgnoreCase("NI") || pocketNum.equalsIgnoreCase("TI") || pocketNum.equalsIgnoreCase("PT")) {
            if (!noMeltSelectedOption) {
                if (!baseWeight.equalsIgnoreCase("") && !mainAddWeight.equalsIgnoreCase("")) {
                    ifCanPrep = true;
                }
            } else {
                ifCanPrep = true;
            }
        }
        return ifCanPrep;
    }
    
    public void expireCredentialsForEvap(String evapId, Map<String, List<MetalUsageObject>> evapToDataLocalMap) {
        if (evapToDataLocalMap.get(evapId) != null) {
            for (List<MetalUsageObject> metalUsageObjectList : evapToDataLocalMap.values()) {
                for (MetalUsageObject metalUsageObject : metalUsageObjectList) {
                    metalUsageObject.setUserEvapCredentials(null);
                }
            }
            evapToDataLocalMap.remove(evapId);
            if (evapToDataLocalMap != null) {
                setEvapToDataMap(evapToDataLocalMap);
            }
        }
    }
    
    public void initSessionID(String loggedInUser) {
        
        usernameLabel.setText(loggedInUser);
        
        n_evap06.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP06");
            }
        });
        
        n_evap07.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP07");
            }
        });
        
        n_evap08.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP08");
            }
        });
        
        n_evap09.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP09");
            }
        });
        
        n_evap10.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP10");
            }
        });
        
        n_evap11.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP11");
            }
        });
        
        n_evap12.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP12");
            }
        });
        
        n_evap13.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP13");
            }
        });
        
        n_evap14.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP14");
            }
        });
        
        n_evap15.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP15");
            }
        });
        
        n_evap16.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP16");
            }
        });
        
        n_evap17.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP17");
            }
        });
        
        n_evap18.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP18");
            }
        });
        n_evap19.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                promptForLogin("N_EVAP19");
            }
        });
    }
    
    public void promptForLogin(String evapId) {
        Credentials creds = null;
        //Check that credentials have not timed out
        if (evapToDataMap.get(evapId.trim()) != null && evapToDataMap.get(evapId.trim()).get(0).getUserEvapCredentials() != null
                && (evapToDataMap.get(evapId.trim()).get(0).getUserEvapCredentials().getLoginTime() + ALLOWABLE_LOGIN_TIME) > System.currentTimeMillis()) {
            try {
                creds = ConfigurationServer.loginUser(evapToDataMap.get(evapId.trim()).get(0).getUserEvapCredentials().getUsername());
            } catch (Exception e) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.INFO, "Not Valid User " + evapToDataMap.get(evapId.trim()).get(0).getUserEvapCredentials().getUsername(), e);
            }
        } else {
            try {
                creds = ConfigurationServer.loginUser("", evapId + " scale");
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(MainViewController.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            evapToDataMap.put(evapId, Arrays.asList(new MetalUsageObject(creds, "AU4"), new MetalUsageObject(creds, "AU3"), new MetalUsageObject(creds, "AUGE"),
                    new MetalUsageObject(creds, "NI"), new MetalUsageObject(creds, "PT"), new MetalUsageObject(creds, "TI")));
            //Start the timers
            evapPauseTransitionManager.scheduleLogin(evapId, evapToDataMap);
            evapRefreshDisplayManager.scheduleForRefresh(evapId, evapToDataMap, data, outLocationValues, prepStatus, tableView);
        }
        
        if (creds != null) {
            populateTable(evapId);   //Store the data into the map, Pass the map with the cached data
            usernameLabel.setText(creds.getUsername().trim());
        }
    }

    /**
     * @param evapId the currentEvapId to set
     */
    public void setEvapId(String evapId) {
        this.currentEvapId = evapId;
    }
    
}
