package com.skyworksinc.evapScalesGateway.Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import com.skyworksinc.cim.ConfigDialogController;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 *
 * @author mattoop
 */
public class EvapScalesController implements Initializable {
    @FXML
    protected Label usernameLabel;
     /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Logger.getLogger(EvapScalesController.class.getName()).log(Level.INFO, "Application Started:");
        Logger.getLogger(EvapScalesController.class.getName()).log(Level.INFO, "Starting Logging.....");
    }
    
     @FXML
    protected void loginAction(ActionEvent event) {
        try {
            // log in
            ConfigDialogController.showDialog(ConfigDialogController.DialogType.LOGIN);

        } catch (IOException ex) {
            Logger.getLogger(EvapScalesController.class.getName()).log(Level.ERROR, null, ex);
        }
        usernameLabel.setText("Unknown");  // we no longer use this field.
    }
}
