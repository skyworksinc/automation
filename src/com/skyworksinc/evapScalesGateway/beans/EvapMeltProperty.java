package com.skyworksinc.evapScalesGateway.beans;

/**
 *
 * @author mattoop
 */
public class EvapMeltProperty {
    private String meltId;
    private String state;
    private String baseWeight;
    private String mainAddWeight;
    private String subAddWeight;
    private String postWeight;
    private String prepId;
    
    
    public EvapMeltProperty(String meltId, String state, String baseWeight, String mainAddWeight, String subAddWeight, String postWeight, String prepId) {
        this.meltId = meltId;
        this.state = state;
        this.baseWeight = baseWeight;
        this.mainAddWeight = mainAddWeight;
        this.subAddWeight = subAddWeight;
        this.postWeight = postWeight;
        this.prepId = prepId;
    }

    /**
     * @return the meltId
     */
    public String getMeltId() {
        return meltId;
    }

    /**
     * @param meltId the meltId to set
     */
    public void setMeltId(String meltId) {
        this.meltId = meltId;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the baseWeight
     */
    public String getBaseWeight() {
        return baseWeight;
    }

    /**
     * @param baseWeight the baseWeight to set
     */
    public void setBaseWeight(String baseWeight) {
        this.baseWeight = baseWeight;
    }

    /**
     * @return the mainAddWeight
     */
    public String getMainAddWeight() {
        return mainAddWeight;
    }

    /**
     * @param mainAddWeight the mainAddWeight to set
     */
    public void setMainAddWeight(String mainAddWeight) {
        this.mainAddWeight = mainAddWeight;
    }

    /**
     * @return the subAddWeight
     */
    public String getSubAddWeight() {
        return subAddWeight;
    }

    /**
     * @param subAddWeight the subAddWeight to set
     */
    public void setSubAddWeight(String subAddWeight) {
        this.subAddWeight = subAddWeight;
    }

    /**
     * @return the postWeight
     */
    public String getPostWeight() {
        return postWeight;
    }

    /**
     * @param postWeight the postWeight to set
     */
    public void setPostWeight(String postWeight) {
        this.postWeight = postWeight;
    }

    /**
     * @return the prepId
     */
    public String getPrepId() {
        return prepId;
    }

    /**
     * @param prepId the prepId to set
     */
    public void setPrepId(String prepId) {
        this.prepId = prepId;
    }
    
}
