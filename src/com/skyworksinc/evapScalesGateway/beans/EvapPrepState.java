package com.skyworksinc.evapScalesGateway.beans;

/**
 *
 * @author mattoop
 */
public class EvapPrepState {
    private String prepId;
    private String state;
    private String meltId;
    private String pocketName;
    
    public EvapPrepState(String prepId, String state, String meltId, String pocketName) {
        this.prepId = prepId;
        this.state = state;
        this.meltId = meltId;
        this.pocketName = pocketName;
    }

    /**
     * @return the prepId
     */
    public String getPrepId() {
        return prepId;
    }

    /**
     * @param prepId the prepId to set
     */
    public void setPrepId(String prepId) {
        this.prepId = prepId;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the meltId
     */
    public String getMeltId() {
        return meltId;
    }

    /**
     * @param meltId the meltId to set
     */
    public void setMeltId(String meltId) {
        this.meltId = meltId;
    }

    /**
     * @return the pocketName
     */
    public String getPocketName() {
        return pocketName;
    }

    /**
     * @param pocketName the pocketName to set
     */
    public void setPocketName(String pocketName) {
        this.pocketName = pocketName;
    }
            
}
