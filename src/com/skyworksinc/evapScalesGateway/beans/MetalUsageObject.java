package com.skyworksinc.evapScalesGateway.beans;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.DoubleProperty;
import java.util.Vector;
import javafx.scene.control.TextField;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;
import com.skyworksinc.cim.promis.transactions.Credentials;
import java.util.ArrayList;
import javafx.beans.property.ListProperty;
import java.util.List;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;

/**
 *
 * @author mattoop
 */
public class MetalUsageObject {

    private BooleanProperty makeMelt;
    private BooleanProperty swapMelt;
    private BooleanProperty noMelt;
    private StringProperty outMeltId;
    private StringProperty meltWeight;
    private StringProperty inMeltId;
    private StringProperty inLocation;
    private StringProperty preAdd;
    private StringProperty postAdd;
    private StringProperty qtyLgPellet;
    private StringProperty qtySmPellet;
    private StringProperty metal;
    private StringProperty pocket;
    private StringProperty metalLargePlt;
    private StringProperty metalSmallPlt;
    private BooleanProperty isMeltIdentified;
    private StringProperty meltState;
    private BooleanProperty isQualifiedState;
    private Credentials userEvapCredentials;
    private ListProperty outLocation;
    private BooleanProperty isPreppedWithNoMelt;
  //  private Button cancelPrepButton;

    public MetalUsageObject(boolean makeMelt, boolean swapMelt, boolean noMelt, String outMeltId, String meltWeight, String inMeltId, String inLocation,
            String preAdd, String postAdd, String qtyLgPellet, String qtySmPellet, String metal, String pocket, String metalLargePlt, String metalSmallPlt, boolean isMeltIdentified,
            String meltState, boolean isQualifiedState, List<String> outLocationList, boolean isPreppedWithNoMelt) {
        this.makeMelt = new SimpleBooleanProperty(makeMelt);
        this.swapMelt = new SimpleBooleanProperty(swapMelt);
        this.noMelt = new SimpleBooleanProperty(noMelt);
        this.outMeltId = new SimpleStringProperty(outMeltId);
        this.meltWeight = new SimpleStringProperty(meltWeight);
        this.inMeltId = new SimpleStringProperty(inMeltId);
        this.inLocation = new SimpleStringProperty(inLocation);
        this.preAdd = new SimpleStringProperty(preAdd);
        this.postAdd = new SimpleStringProperty(postAdd);
        this.qtyLgPellet = new SimpleStringProperty(qtyLgPellet);
        this.qtySmPellet = new SimpleStringProperty(qtySmPellet);
        this.metal = new SimpleStringProperty(metal);
        this.pocket = new SimpleStringProperty(pocket);
        this.metalLargePlt = new SimpleStringProperty(metalLargePlt);
        this.metalSmallPlt = new SimpleStringProperty(metalSmallPlt);
        this.isMeltIdentified = new SimpleBooleanProperty(isMeltIdentified);
        this.meltState = new SimpleStringProperty(meltState);
        this.isQualifiedState = new SimpleBooleanProperty(isQualifiedState);
        ObservableList<String> observableOutLocationList = FXCollections.observableArrayList(outLocationList);
        this.outLocation = new SimpleListProperty(observableOutLocationList);
        this.isPreppedWithNoMelt = new SimpleBooleanProperty(isPreppedWithNoMelt);
    //    this.cancelPrepButton = new Button("Cancel Prep");
    }

    public MetalUsageObject(boolean makeMelt, boolean swapMelt, boolean noMelt, String outMeltId, String meltWeight, String inMeltId, String inLocation,
            String preAdd, String postAdd, String qtyLgPellet, String qtySmPellet, String metal, String pocket, String metalLargePlt, String metalSmallPlt, boolean isMeltIdentified,
            String meltState, boolean isQualifiedState, List<String> outLocationList, boolean isPreppedWithNoMelt, Credentials userEvapCredentials) {
        this(makeMelt, swapMelt, noMelt, outMeltId, meltWeight, inMeltId, inLocation,
            preAdd, postAdd, qtyLgPellet, qtySmPellet, metal, pocket, metalLargePlt, metalSmallPlt, isMeltIdentified,
            meltState, isQualifiedState, outLocationList, isPreppedWithNoMelt);
        this.userEvapCredentials = userEvapCredentials;
    }
    
    public MetalUsageObject(Credentials userEvapCredentials, String metal) {
        this(false, false, false, null, null, null, null,
            null, null, null, null, metal, null, null, null, false,
            null, false, new ArrayList<>(), false);
        this.userEvapCredentials = userEvapCredentials;
    }
    
    public StringProperty pocketProperty() {
        return this.pocket;
    }
    
    public StringProperty metalProperty() {
        return this.metal;
    }
    /**
     *
     * @return the makeMelt
     */
    public BooleanProperty makeMeltProperty() {
        return this.makeMelt;
    }

    /**
     * @return the swapMelt
     */
    public BooleanProperty swapMeltProperty() {
        return this.swapMelt;
    }

    /**
     * @return the noMelt
     */
    public BooleanProperty noMeltProperty() {
        return this.noMelt;
    }

    /**
     * @return the outMeltId
     */
    public StringProperty outMeltIdProperty() {
        return this.outMeltId;
    }

    public StringProperty meltWeightProperty() {
        return this.meltWeight;
    }

    public StringProperty inMeltIdProperty() {
        return this.inMeltId;
    }

    public StringProperty inLocationProperty() {
        return this.inLocation;
    }
    
    public ListProperty outLocationProperty() {
        return this.outLocation;
    }
    
    public StringProperty preAddProperty() {
        return this.preAdd;
    }

    public StringProperty postAddProperty() {
        return this.postAdd;
    }

    public StringProperty qtyLgPelletProperty() {
        return this.qtyLgPellet;
    }

    public StringProperty qtySmPelletProperty() {
        return this.qtySmPellet;
    }
    
    public StringProperty metalLargePltProperty() {
        return this.metalLargePlt;
    }
    
    public StringProperty metalSmallPltProperty() {
        return this.metalSmallPlt;
    }
    
    public BooleanProperty isMeltIdentifiedProperty() {
        return this.isMeltIdentified;
    }
    
    public BooleanProperty isQualifiedStateProperty() {
        return this.isQualifiedState;
    }
    
    public BooleanProperty isPreppedWithNoMeltProperty() {
        return this.isPreppedWithNoMelt;
    }
    
    public StringProperty meltStateProperty() {
        return this.meltState;
    }

    public boolean getMakeMelt() {
        return makeMelt.get();
    }

    public boolean getSwapMelt() {
        return swapMelt.get();
    }

    public boolean getNoMelt() {
        return noMelt.get();
    }

    public String getOutMeltId() {
        return outMeltId.get();
    }

    /**
     * @return the meltWeight
     */
    public String getMeltWeight() {
        return meltWeight.get();
    }

    /**
     * @return the inMeltId
     */
    public String getInMeltId() {
        return inMeltId.get();
    }

    /**
     * @return the inLocation
     */
    public String getInLocation() {
        return inLocation.get();
    }
    
    /**
     * @return the outLocation
     */
    public Object getOutLocation() {
        return outLocation.get();
    }

    /**
     * @return the preAdd
     */
    public String getPreAdd() {
        return preAdd.get();
    }

    /**
     * @return the postAdd
     */
    public String getPostAdd() {
        return postAdd.get();
    }

    /**
     * @return the qtyLgPellet
     */
    public String getQtyLgPellet() {
        return qtyLgPellet.get();
    }

    /**
     * @return the qtySmPellet
     */
    public String getQtySmPellet() {
        return qtySmPellet.get();
    }

    public String getMetal() {
        return metal.get();
    }

    public String getPocket() {
        return pocket.get();
    }
    
    public String getMetalLargePlt() {
        return metalLargePlt.get();
    }
    
    public String getMetalSmallPlt() {
        return metalSmallPlt.get();
    }
    
    public boolean getIsMeltIdentified() {
        return isMeltIdentified.get();
    }
    
    public boolean getIsQualifiedState() {
        return isQualifiedState.get();
    }
    
    public boolean getIsPreppedWithNoMelt() {
        return isPreppedWithNoMelt.get();
    }

    public String getMeltState() {
        return meltState.get();
    }

    /**
     * @param makeMelt the makeMelt to set
     */
    public void setMakeMelt(boolean makeMelt) {
        this.makeMelt.set(makeMelt);
    }

    /**
     * @param swapMelt the swapMelt to set
     */
    public void setSwapMelt(boolean swapMelt) {
        this.swapMelt.set(swapMelt);
    }

    /**
     * @param noMelt the noMelt to set
     */
    public void setNoMelt(boolean noMelt) {
        this.noMelt.set(noMelt);
    }

    /**
     * @param outMeltIdAU4 the outMeltId to set
     */
    public void setOutMeltId(String outMeltId) {
        this.outMeltId.set(outMeltId);
    }

    /**
     * @param meltWeight the meltWeight to set
     */
    public void setMeltWeight(String meltWeight) {
        this.meltWeight.set(meltWeight);
    }

    /**
     * @param inMeltId the inMeltId to set
     */
    public void setInMeltId(String inMeltId) {
        this.inMeltId.set(inMeltId);
    }

    /**
     * @param inLocation the inLocation to set
     */
    public void setInLocation(String inLocation) {
        this.inLocation.set(inLocation);
    }
    
    /**
     * @param outLocation the outLocation to set
     */
    public void setOutLocation(List outLocation) { 
        this.outLocation.set(outLocation);
    }

    /**
     * @param preAdd the preAdd to set
     */
    public void setPreAdd(String preAdd) {
        this.preAdd.set(preAdd);
    }

    /**
     * @param postAdd the postAdd to set
     */
    public void setPostAdd(String postAdd) {
        this.postAdd.set(postAdd);
    }

    /**
     * @param qtyLgPellet the qtyLgPellet to set
     */
    public void setQtyLgPellet(String qtyLgPellet) {
        this.qtyLgPellet.set(qtyLgPellet);
    }

    /**
     * @param qtySmPellet the qtySmPellet to set
     */
    public void setQtySmPellet(String qtySmPellet) {
        this.qtySmPellet.set(qtySmPellet);
    }
    
    public void setMetal(String metal) {
        this.metal.set(metal);
    }
    
    public void setPocket(String pocket) {
        this.pocket.set(pocket);
    }
    
    public void setMetalLargePlt(String metalLargePlt) {
        this.metalLargePlt.set(metalLargePlt);
    }
    
    public void setMetalSmallPlt(String metalSmallPlt) {
        this.metalSmallPlt.set(metalSmallPlt);
    }
    
    public void setIsMeltIdentified(boolean isMeltIdentified) {
        this.isMeltIdentified.set(isMeltIdentified);
    }
    
    public void setIsQualifiedState(boolean isQualifiedState) {
        this.isQualifiedState.set(isQualifiedState);
    }
    
     public void setIsPreppedWithNoMelt(boolean isPreppedWithNoMelt) {
        this.isPreppedWithNoMelt.set(isPreppedWithNoMelt);
    }
    
    public void setMeltState(String meltState) {
        this.meltState.set(meltState);
    }

    /**
     * @return the userEvapCredentials
     */
    public Credentials getUserEvapCredentials() {
        return userEvapCredentials;
    }

    /**
     * @param userEvapCredentials the userEvapCredentials to set
     */
    public void setUserEvapCredentials(Credentials userEvapCredentials) {
        this.userEvapCredentials = userEvapCredentials;
    }

}
