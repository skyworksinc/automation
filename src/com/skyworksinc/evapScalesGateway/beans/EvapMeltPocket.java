package com.skyworksinc.evapScalesGateway.beans;

/**
 *
 * @author mattoop
 */
public class EvapMeltPocket {
    private String meltId;
    private String pocketNumber;
    
    public EvapMeltPocket(String meltId, String pocketNumber) {
        this.meltId = meltId;
        this.pocketNumber = pocketNumber;
    }

    /**
     * @return the meltId
     */
    public String getMeltId() {
        return meltId;
    }

    /**
     * @param meltId the meltId to set
     */
    public void setMeltId(String meltId) {
        this.meltId = meltId;
    }

    /**
     * @return the pocketNumber
     */
    public String getPocketNumber() {
        return pocketNumber;
    }

    /**
     * @param pocketNumber the pocketNumber to set
     */
    public void setPocketNumber(String pocketNumber) {
        this.pocketNumber = pocketNumber;
    }
}
