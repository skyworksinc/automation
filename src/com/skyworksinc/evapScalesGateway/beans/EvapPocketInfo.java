package com.skyworksinc.evapScalesGateway.beans;

/**
 *
 * @author mattoop
 */
public class EvapPocketInfo {
    private String eqpId;
    private String pocketName;
    private String meltId;
    private String timestamp;
    private String state;
    private String prepId;
    private double baseWeight;
    private double mainAddWeight;
    private double subAddWeight;
    
    public EvapPocketInfo(String eqpId, String pocketName, String meltId, String timestamp, String state, String prepId, double baseWeight, double mainAddWeight, double subAddWeight) {
        this.eqpId = eqpId;
        this.pocketName = pocketName;
        this.meltId = meltId;
        this.timestamp = timestamp;
        this.state = state;
        this.prepId = prepId;
        this.baseWeight = baseWeight;
        this.mainAddWeight = mainAddWeight;
        this.subAddWeight = subAddWeight;
    }

    /**
     * @return the eqpId
     */
    public String getEqpId() {
        return eqpId;
    }

    /**
     * @param eqpId the eqpId to set
     */
    public void setEqpId(String eqpId) {
        this.eqpId = eqpId;
    }

    /**
     * @return the pocketName
     */
    public String getPocketName() {
        return pocketName;
    }

    /**
     * @param pocketName the pocketName to set
     */
    public void setPocketName(String pocketName) {
        this.pocketName = pocketName;
    }

    /**
     * @return the meltId
     */
    public String getMeltId() {
        return meltId;
    }

    /**
     * @param meltId the meltId to set
     */
    public void setMeltId(String meltId) {
        this.meltId = meltId;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the prepId
     */
    public String getPrepId() {
        return prepId;
    }

    /**
     * @param prepId the prepId to set
     */
    public void setPrepId(String prepId) {
        this.prepId = prepId;
    }

    /**
     * @return the baseWeight
     */
    public double getBaseWeight() {
        return baseWeight;
    }

    /**
     * @param baseWeight the baseWeight to set
     */
    public void setBaseWeight(double baseWeight) {
        this.baseWeight = baseWeight;
    }

    /**
     * @return the mainAddWeight
     */
    public double getMainAddWeight() {
        return mainAddWeight;
    }

    /**
     * @param mainAddWeight the mainAddWeight to set
     */
    public void setMainAddWeight(double mainAddWeight) {
        this.mainAddWeight = mainAddWeight;
    }

    /**
     * @return the subAddWeight
     */
    public double getSubAddWeight() {
        return subAddWeight;
    }

    /**
     * @param subAddWeight the subAddWeight to set
     */
    public void setSubAddWeight(double subAddWeight) {
        this.subAddWeight = subAddWeight;
    }
}
