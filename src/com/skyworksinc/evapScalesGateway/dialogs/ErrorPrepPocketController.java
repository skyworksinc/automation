package com.skyworksinc.evapScalesGateway.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author mattoop
 */
public class ErrorPrepPocketController implements Initializable {

    @FXML
    protected Button okButton;

    /** The dialog controller.  This is used to close the dialog. */
    public static ErrorPrepPocketController controller;

    /**
     * The current dialog or null if no dialog is showing.
     */
    public static Stage stage;
    @FXML
    private Label detailsLabel;
    @FXML
    private Font x1;
    @FXML
    private HBox actionParent;
    @FXML
    private HBox okParent;
    @FXML
    private Label meltIdLabel;
    @FXML
    private Label messageLabel;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void handleCancelClearLotButtonAction(ActionEvent event) {
        close();
    }

    @FXML
    private void handleokContinueButtonAction(ActionEvent event) {
        close();
    }

    public void close() {
        stage.close();
        stage = null;
    }

    /** Close the dialog.
     */
    public static void closeDialog() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.close();
            }
        });
    }

    /** Cause the dialog to be displayed on the event thread. 
     * @param prepStatusMessage 
     */
    public static void showDialog(final String prepStatusMessage, final String detailedMessage) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                    showDialogInternal(prepStatusMessage, detailedMessage);
            }
        });
    }


    /**
     * Show the dialog with the provided parameters.  
     * @param meltId 
     */
    public static void showDialogInternal(final String meltId, final String detailedMsg) {
        if (stage == null) {
            try {

                FXMLLoader loader = new FXMLLoader(ErrorPrepPocketController.class.getResource("ErrorPrepPocket.fxml"));

                stage = new Stage(StageStyle.UNDECORATED);
                stage.setScene(new Scene((Pane) loader.load()));

                controller = loader.<ErrorPrepPocketController>getController();
                controller.meltIdLabel.setText(meltId);
                controller.detailsLabel.setText(detailedMsg);
                controller.messageLabel.setText("WARNING: " + detailedMsg);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setResizable(false);
                stage.sizeToScene();
                stage.centerOnScreen();
                stage.showAndWait();
            } catch (Exception any) {
                any.printStackTrace();
                Logger.getLogger(ErrorPrepPocketController.class.getName()).log(Level.ERROR, null, any);
                stage = null;
            }
        }
    }

}
